package ekylibre.service;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import org.apache.commons.lang3.StringUtils;

import ekylibre.database.DatabaseHelper;
import ekylibre.database.ZeroContract.DetailedInterventionAttributes;
import ekylibre.database.ZeroContract.Products;
import ekylibre.database.ZeroContract.Variants;
import ekylibre.zero.R;

import static ekylibre.zero.home.MainActivity.CHANNEL_ID;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 */
public class LongMigrationService extends IntentService {

    private static final String ACTION_SEARCH_NAME = "ekylibre.service.action.migrate.SEARCH_NAMES";
    private static final String ACTION_UNITS = "ekylibre.service.action.migrate.UNITS";
    private static final String SEARCH_NAME_COLUMN = "search_name";
    private static final int notificationId = 246810;  // This is an arbitrary id
    private ContentResolver cr;
    private SQLiteDatabase db;
    private NotificationManagerCompat notificationManager;
    private NotificationCompat.Builder builder;

    public LongMigrationService() {
        super("LongMigrationService");
    }

    /**
     * Starts this service to perform action SearchNames with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startActionSearchNames(Context context) {
        Intent intent = new Intent(context, LongMigrationService.class);
        intent.setAction(ACTION_SEARCH_NAME);
        context.startService(intent);
    }

    public static void startActionMigrateUnits(Context context) {
        Intent intent = new Intent(context, LongMigrationService.class);
        intent.setAction(ACTION_UNITS);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_SEARCH_NAME.equals(action))
                handleActionSearchNames();
            else if (ACTION_UNITS.equals(action))
                handleActionMigrateUnits();
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionSearchNames() {

        cr = getContentResolver();
        db = new DatabaseHelper(this).getReadableDatabase();

        // Notification builder
        notificationManager = NotificationManagerCompat.from(this);
        builder = new NotificationCompat.Builder(this, CHANNEL_ID);
        builder.setContentTitle("Maintenance en cours")
                .setSmallIcon(R.drawable.ic_migration)
                .setPriority(NotificationCompat.PRIORITY_LOW);

        computeSearchNames(Products.TABLE_NAME);
        computeSearchNames(Variants.TABLE_NAME);

        // Set as done in shared prefs
        final SharedPreferences prefs = getSharedPreferences("prefs", Context.MODE_MULTI_PROCESS);
        prefs.edit().putBoolean("are_names_normalized_for_search", true).apply();

        // When done, update the notification one more time to remove the progress bar
//        builder.setContentTitle("Maintenance terminée !")
//                .setContentText("Vous pouvez fermer cette notification.")
//                .setProgress(0,0,false);
//        notificationManager.notify(notificationId, builder.build());

        notificationManager.cancel(notificationId);
    }

    private void computeSearchNames(String tableName) {

        Uri uri = tableName.equals(Products.TABLE_NAME) ? Products.CONTENT_URI : Variants.CONTENT_URI;
        final String where = "ek_id=?";

        int PROGRESS_CURRENT = 0;
        int PROGRESS_MAX = (int) DatabaseUtils.queryNumEntries(db, tableName);

        // Customise notification and reset
        builder.setContentText(String.format("optimisation de la table %s", tableName))
                .setProgress(PROGRESS_MAX, PROGRESS_CURRENT, false);
        notificationManager.notify(notificationId, builder.build());

        // Do the job
        try (Cursor cursor = cr.query(uri, Products.RENAME_PROJ,
                null, null, null)) {

            while (cursor != null && cursor.moveToNext()) {

                ContentValues cv = new ContentValues();
                String[] arg = new String[]{cursor.getString(0)};
                String string = StringUtils.stripAccents(cursor.getString(1));
                cv.put(SEARCH_NAME_COLUMN, string);

                cr.update(uri, cv, where, arg);

                if (++PROGRESS_CURRENT % 100 == 0) {
                    builder.setProgress(PROGRESS_MAX, PROGRESS_CURRENT, false);
                    notificationManager.notify(notificationId, builder.build());
                }
            }
        }

    }

    private void handleActionMigrateUnits() {

//        cr = getContentResolver();
//
//        // Do the job
//        try (Cursor cursor = cr.query(DetailedInterventionAttributes.CONTENT_URI,
//                DetailedInterventionAttributes.PROJECTION_ALL,
//                "quantity_indicator IS NOT NULL", null, null)) {
//
//            if (cursor == null)
//                return;
//
//            while (cursor.moveToNext()) {
//
//                ContentValues cv = new ContentValues();
//                String[] arg = new String[]{cursor.getString(0)};
//                String string = StringUtils.stripAccents(cursor.getString(1));
//                cv.put(SEARCH_NAME_COLUMN, string);
//
//                cr.update(uri, cv, where, arg);
//            }
//        }
    }


}
