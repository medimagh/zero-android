package ekylibre.service;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountsException;
import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.util.Pair;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

import ekylibre.APICaller.DetailedIntervention;
import ekylibre.APICaller.EphyCropset;
import ekylibre.APICaller.Instance;
import ekylibre.APICaller.Intervention;
import ekylibre.APICaller.Issue;
import ekylibre.APICaller.Observation;
import ekylibre.APICaller.PhytosanitaryReference;
import ekylibre.APICaller.PhytosanitaryUsage;
import ekylibre.APICaller.Plant;
import ekylibre.APICaller.PlantCounting;
import ekylibre.APICaller.PlantDensityAbacus;
import ekylibre.APICaller.Product;
import ekylibre.APICaller.Variant;
import ekylibre.database.DatabaseHelper;
import ekylibre.database.ZeroContract.EphyCropsets;
import ekylibre.database.ZeroContract.Crumbs;
import ekylibre.database.ZeroContract.DetailedInterventionAttributes;
import ekylibre.database.ZeroContract.DetailedInterventions;
import ekylibre.database.ZeroContract.GroupWorks;
import ekylibre.database.ZeroContract.GroupZones;
import ekylibre.database.ZeroContract.InterventionParameters;
import ekylibre.database.ZeroContract.Interventions;
import ekylibre.database.ZeroContract.Issues;
import ekylibre.database.ZeroContract.ObservationIssues;
import ekylibre.database.ZeroContract.Observations;
import ekylibre.database.ZeroContract.PhytosanitaryReferences;
import ekylibre.database.ZeroContract.PhytosanitaryUsages;
import ekylibre.database.ZeroContract.PlantCountingItems;
import ekylibre.database.ZeroContract.PlantCountings;
import ekylibre.database.ZeroContract.PlantDensityAbaci;
import ekylibre.database.ZeroContract.PlantDensityAbacusItems;
import ekylibre.database.ZeroContract.Plants;
import ekylibre.database.ZeroContract.Products;
import ekylibre.database.ZeroContract.Variants;
import ekylibre.database.ZeroContract.WorkingPeriodAttributes;
import ekylibre.database.ZeroContract.WorkingPeriods;
import ekylibre.exceptions.HTTPException;
import ekylibre.util.AccountTool;
import ekylibre.util.ImageConverter;
import ekylibre.util.NetworkHelpers;
import ekylibre.zero.BuildConfig;
import io.sentry.core.Sentry;
import io.sentry.core.protocol.User;

import static ekylibre.database.ZeroContract.AUTHORITY;
import static ekylibre.util.Helper.getTranslation;
import static ekylibre.util.Helper.iso8601date;
import static ekylibre.util.Helper.toIso8601Android;
import static ekylibre.zero.home.MainActivity.CHANNEL_ID;
import static ekylibre.zero.home.Zero.ISO8601;
import static ekylibre.zero.home.Zero.LARRERE;
import static ekylibre.zero.inter.InterActivity.FINISHED;
import static ekylibre.zero.intervention.InterventionActivity.STATUS_FINISHED;
import static ekylibre.zero.intervention.InterventionActivity.STATUS_IN_PROGRESS;
import static ekylibre.zero.intervention.InterventionActivity.STATUS_PAUSE;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 */
public class LongSyncService extends JobIntentService {

    private static final String TAG = "LongSyncService";

    public static final int JOB_ID = 1232123;
    public static final String ACTION_SYNC_ALL = "ekylibre.service.action.sync.ALL";
    public static final String RETRY_FAILED = "retry_failed";
    public static final int SYNC_FINISHED = 123;
    public static final int SYNC_FINISHED_WITH_ERRORS = 124;
    public static final int NO_NETWORK = 132;

    private static final int NOTIFICATION_ID = 2468109;  // This is an arbitrary id
    private static final int BULK_SIZE = 50;
    private static final String SYNCED = "SYNCED";

    private ContentResolver cr;
    private SQLiteDatabase db;
    private Account account;
    private String accountURL;
    private Instance instance;
    private NotificationManagerCompat notificationManager;
    private NotificationCompat.Builder builder;
    private SharedPreferences prefs;
    private ResultReceiver receiver;
    private boolean retryFailed;
    private boolean hasErrors;
    private String lastSyncattribute;
    private int PROGRESS_MAX;
    private int PROGRESS_CURRENT;

    public static void enqueueWork(Context context, Intent work) {
        enqueueWork(context, LongSyncService.class, JOB_ID, work);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        retryFailed = intent.getBooleanExtra(RETRY_FAILED, false);
        final String action = intent.getAction();
        receiver = intent.getParcelableExtra("receiver");
        if (ACTION_SYNC_ALL.equals(action))
            handleActionSync();
    }

    /**
     * Handle action Sync in the provided background thread with the provided
     * parameters.
     */
    private void handleActionSync() {

        // Check wether network is available or not
        if (!NetworkHelpers.isNetworkAvailable(this)) {
            // Schedule retry in 10 minutes
            postponeSync(10);
            if (receiver != null)
                receiver.send(NO_NETWORK, new Bundle());
            return;
        }

        long startDate = new Date().getTime();

        // Update certs to avoid HandshakeSSLError
        NetworkHelpers.fixHandshakeSSLError(this);

        // Init all account related variables
        cr = getContentResolver();
        account = AccountTool.getCurrentAccount(this);
        Log.d(TAG, "account.name == " + account.name);
        accountURL = AccountTool.getInstance(account);
        instance = getInstance();

        prefs = getSharedPreferences("prefs", Context.MODE_MULTI_PROCESS);
        db = new DatabaseHelper(this).getReadableDatabase();

        // Init error flag used for user notification
        hasErrors = false;

        // Sentry Logger - Set the user in the current context.
        User user = new User();
        user.setEmail(AccountTool.getEmail(account));
        user.setId(accountURL);
        Sentry.setUser(user);

        // Creates notification builder
        notificationManager = NotificationManagerCompat.from(this);
        builder = new NotificationCompat.Builder(this, CHANNEL_ID);
        builder.setContentTitle("Synchronisation")
                .setSmallIcon(android.R.drawable.ic_popup_sync)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setWhen(System.currentTimeMillis())
                .setOngoing(true);

        // Get current user workerId if exists
        int workerId = getWorkerId();
        lastSyncattribute = getLastSyncAttribute();

        // Sync tasks

        postInterventions();
        if (workerId != -1)
            getInterventions();

        // This features are only available for larrere flavor
        if (BuildConfig.FLAVOR.equals(LARRERE)) {
            getPlantDensityAbaci();
            postIssues();
            postPlantCountings();
            getPlants();
            postObservations();
        }
        getProducts();
        getVariants();
        postDetailedInterventions();

//        if (prefs.getBoolean("sync_lexicon", true)) {
//            getLexiconPhytosanitaryReferences();
//            getLexiconPhytosanitaryUsages();
//            getEphyCropsets();
//            prefs.edit().putBoolean("sync_lexicon", false).apply();
//        }

        // When done, remove the notification
        notificationManager.cancel(NOTIFICATION_ID);

        // Set as done in shared prefs
        prefs.edit()
                .putBoolean("are_names_normalized_for_search", true)
                .putLong("last_sync_date_" + account.name, startDate)
                .apply();

        // Send finish info to MainActivity
        if (receiver != null)
            receiver.send(hasErrors && retryFailed ?
                    SYNC_FINISHED_WITH_ERRORS : SYNC_FINISHED, new Bundle());
    }

    private void postIssues() {

        setNotification("Synchro. des incidents");

        final String where = "user == ? AND synced == 0";
        String[] args = new String[]{account.name};

        // Query database for issues
        try (Cursor cursor = cr.query(Issues.CONTENT_URI, Issues.PROJECTION_ALL,
                where, args, Issues.SORT_ORDER_DEFAULT)) {

            if (cursor == null)
                return;

            while (cursor.moveToNext()) {
                try {
                    // Prepare the JSON data
                    JSONObject payload = new JSONObject();
                    payload.put("nature", cursor.getString(1));
                    payload.put("gravity", cursor.getInt(2));
                    payload.put("priority", cursor.getInt(3));
                    payload.put("description", cursor.getString(5));
                    payload.put("observed_at", ISO8601.format(new Date(cursor.getLong(8))));
                    if (cursor.getDouble(9) != 0 && cursor.getDouble(10) != 0)
                        payload.put("geolocation", String.format("SRID=4326; POINT(%s %s)",
                                cursor.getDouble(10), cursor.getDouble(9)));

                    // Request Issue creation on server
                    long id = Issue.create(instance, payload);

                    String db_id = Long.toString(cursor.getLong(0));

                    // Marks them as synced
                    ContentValues values = new ContentValues();
                    values.put(Issues.SYNCED, 1);
                    cr.update(Uri.withAppendedPath(Issues.CONTENT_URI, db_id), values, null, null);

                    values.clear();
                    values.put(ObservationIssues.EKY_ID_ISSUE, id);
                    cr.update(ObservationIssues.CONTENT_URI, values, "fk_issue = ?", new String[]{db_id});

                } catch (JSONException | IOException | HTTPException e) {
                    Sentry.captureException(e);
                    e.printStackTrace();
                }
            }
        }
    }

    private void getPlantDensityAbaci() {

        setNotification("Synchro. des abaques");

        // Delete tables
        cr.delete(PlantDensityAbacusItems.CONTENT_URI, null, null);
        cr.delete(PlantDensityAbaci.CONTENT_URI, null, null);
        // TODO :: update items instead of dropping them

        // Init empty list
        List<PlantDensityAbacus> abacusList = null;

        // Do the API call
        try {
            abacusList = PlantDensityAbacus.all(instance, null);
        } catch (JSONException | IOException | HTTPException e) {
            Sentry.captureException(e);
            e.printStackTrace();
        }

        // Finish if no abaci
        if (abacusList == null)
            return;

        ArrayList<ContentProviderOperation> operations = new ArrayList<>();

        PROGRESS_MAX = abacusList.size();
        PROGRESS_CURRENT = 0;

        // Customise notification and reset
        updateNotificationProgress();

        for (PlantDensityAbacus abacus : abacusList) {

            ContentValues cv = new ContentValues();
            cv.put(PlantDensityAbaci.EK_ID, abacus.getId());
            cv.put(PlantDensityAbaci.NAME, abacus.getName());
            cv.put(PlantDensityAbaci.VARIETY, abacus.getVariety());
            cv.put(PlantDensityAbaci.ACTIVITY_ID, abacus.getActivityID());
            cv.put(PlantDensityAbaci.GERMINATION_PERCENTAGE, abacus.getGerminationPercentage());
            cv.put(PlantDensityAbaci.SEEDING_DENSITY_UNIT, abacus.getSeedingDensityUnit());
            cv.put(PlantDensityAbaci.SAMPLING_LENGTH_UNIT, abacus.getSamplingLenghtUnit());
            cv.put(PlantDensityAbaci.USER, account.name);

            operations.add(ContentProviderOperation.newInsert(
                    PlantDensityAbaci.CONTENT_URI).withValues(cv).build());

            ++PROGRESS_CURRENT;

            for (int i=0; i<abacus.mItems.length(); i++) {

                try {
                    cv.clear();
                    cv.put(PlantDensityAbacusItems.EK_ID, abacus.getItemID(i));
                    cv.put(PlantDensityAbacusItems.FK_ID, abacus.getId());
                    cv.put(PlantDensityAbacusItems.PLANTS_COUNT, abacus.getItemPlantCount(i));
                    cv.put(PlantDensityAbacusItems.SEEDING_DENSITY_VALUE, abacus.getItemDensityValue(i));
                    cv.put(PlantDensityAbacusItems.USER, account.name);

                    operations.add(ContentProviderOperation.newInsert(
                            PlantDensityAbacusItems.CONTENT_URI).withValues(cv).build());

                } catch (JSONException e) {
                    Sentry.captureException(e);
                    e.printStackTrace();
                }
            }
        }

        // Bulk insert
        bulkInsert(operations);
    }

    private void postPlantCountings() {

        setNotification("Synchro. des comptages");

        final String where = "user == ? AND synced == 0";
        String[] args = new String[]{account.name};

        // Query database for plant countings
        try (Cursor cursor = cr.query(PlantCountings.CONTENT_URI, PlantCountings.PROJECTION_ALL,
                where, args, PlantCountings.SORT_ORDER_DEFAULT)) {

            // Finish if no countings
            if (cursor == null)
                return;

            while (cursor.moveToNext()) {
                try {
                    JSONObject payload = new JSONObject();
                    payload.put("read_at", ISO8601.format(new Date(cursor.getLong(1))));
                    payload.put("comment", cursor.getString(4));
                    payload.put("plant_density_abacus_item_id", cursor.getInt(5));
                    payload.put("plant_id", cursor.getInt(8));
                    payload.put("average_value", cursor.getFloat(9));
                    payload.put("nature", cursor.getString(10));
                    payload.put("items_attributes", _createPlantCountingItemJSON(cursor.getInt(0)));

                    // Request counting creation on server
                    PlantCounting.create(instance, payload);

                    ContentValues values = new ContentValues();
                    values.put(PlantCountings.SYNCED, 1);
                    cr.update(Uri.withAppendedPath(
                            PlantCountings.CONTENT_URI, Long.toString(cursor.getLong(0))),
                            values, null, null);

                } catch (Exception e) {
                    Sentry.captureException(e);
                    e.printStackTrace();
                }
            }
        }
    }

    private JSONArray _createPlantCountingItemJSON(int plantCountingId) {

        final String where = "user == ? AND plant_counting_id == ?";
        String[] args = new String[]{account.name, String.valueOf(plantCountingId)};

        try (Cursor cursor = cr.query(PlantCountingItems.CONTENT_URI, PlantCountingItems.PROJECTION_ALL,
                where, args, PlantCountingItems.SORT_ORDER_DEFAULT)) {

            if (cursor == null)
                return null;

            JSONArray arrayJSON = new JSONArray();

            while (cursor.moveToNext()) {
                JSONObject attributes = new JSONObject();
                attributes.put("value", cursor.getInt(1));
                arrayJSON.put(attributes);
            }
            return arrayJSON;

        } catch (JSONException e) {
            Sentry.captureException(e);
            e.printStackTrace();
        }
        return null;
    }

    private void postInterventions() {

        setNotification("Synchro. des interventions");

        final String where = String.format("user == ? AND state IN ('%s', '%s', '%s') AND detailed_intervention_id IS NULL",
                STATUS_PAUSE, STATUS_IN_PROGRESS, STATUS_FINISHED);
        String[] args = new String[]{account.name};

        try (Cursor cursor = cr.query(Interventions.CONTENT_URI, Interventions.PROJECTION_POST,
                where, args, Interventions.SORT_ORDER_DEFAULT)) {

            if (cursor == null)
                return;

            while (cursor.moveToNext()) {
                try {
                    _postNewIntervention(cursor);
                } catch (HTTPException | IOException | JSONException e) {
                    Sentry.captureException(e);
                    e.printStackTrace();
                }
            }
        }
    }

    @SuppressLint("HardwareIds")
    private void _postNewIntervention(Cursor cursor) throws JSONException, IOException, HTTPException {

        int interventionId = cursor.getInt(0);

        // Prepare payload
        JSONObject payload = new JSONObject();
        if (cursor.getInt(1) > 0) {
            payload.put("intervention_id", cursor.getInt(1));  // "request_"
            payload.put("request_compliant", cursor.getInt(3));
        }
        payload.put("uuid", cursor.getString(5));

        // Set provider
        JSONObject provider = new JSONObject();
        provider.put("zero_id", cursor.getInt(1));
        payload.put("providers", provider);

        // Set state and hour meter if available
        String state = cursor.getString(4);
        if (state.equals(STATUS_FINISHED)) {
            payload.put("state", "done");
            // Adds hour meter for equipments
            JSONArray hourMeters = _buildHourMeters(interventionId);
            if (hourMeters.length() > 0)
                payload.put("equipments", hourMeters);
        } else
            payload.put("state", "in_progress");

        payload.put("procedure_name", cursor.getString(2));
        payload.put("device_uid", "android:" + Settings.Secure.getString(cr, Settings.Secure.ANDROID_ID));

        JSONArray workingPeriods = _buildWorkingPeriods(interventionId);
        if (workingPeriods != null && workingPeriods.length() != 0)
            payload.put("working_periods", workingPeriods);

        JSONArray crumbs = _buildCrumbs(interventionId);
        if (crumbs != null && crumbs.length() > 0)
            payload.put("crumbs", crumbs);

        if (!cursor.isNull(6))
            payload.put("description", cursor.getString(6));

        // Send the intervention
        long responseId = Intervention.create(instance, payload);

        // Set intervention as well synced if it's the case
        if (state.equals(STATUS_FINISHED) && responseId != -1) {
            ContentValues cv = new ContentValues();
            cv.put(Interventions.STATE, SYNCED);
            Uri uri = Uri.withAppendedPath(Interventions.CONTENT_URI, String.valueOf(interventionId));
            cr.update(uri, cv, null, null);
        }
    }

    private JSONArray _buildHourMeters(int interventionId) throws JSONException {

        final String where = "fk_intervention == ? AND hour_meter IS NOT NULL";
        String[] args = new String[]{String.valueOf(interventionId)};
        JSONArray array = new JSONArray();

        try (Cursor curs = cr.query(InterventionParameters.CONTENT_URI,
                InterventionParameters.PROJECTION_TOOL_FULL, where, args, null)) {

            while (curs != null && curs.moveToNext()) {
                JSONObject obj = new JSONObject();
                obj.put("product_id", curs.getInt(4));
                obj.put("hour_counter", curs.getFloat(5));
                array.put(obj);
            }
        }
        return array;
    }

    private JSONArray _buildWorkingPeriods(int interventionId) {

        final String where = "fk_intervention == ?";
        String[] args = new String[]{String.valueOf(interventionId)};

        try (Cursor cursor = cr.query(WorkingPeriods.CONTENT_URI, WorkingPeriods.PROJECTION_POST,
                where, args, WorkingPeriods.SORT_ORDER_DEFAULT)) {

            if (cursor == null)
                return null;

            JSONArray arrayJSON = new JSONArray();

            while (cursor.moveToNext()) {
                JSONObject attributes = new JSONObject();
                attributes.put("started_at", cursor.getString(1));
                attributes.put("stopped_at", cursor.getString(2));
                attributes.put("nature", cursor.getString(3));
                arrayJSON.put(attributes);
            }
            return arrayJSON;

        } catch (Exception e) {
            Sentry.captureException(e);
            e.printStackTrace();
        }
        return null;
    }

    private JSONArray _buildCrumbs(int interventionId) {

        final String where = "fk_intervention == ?";
        String[] args = new String[]{String.valueOf(interventionId)};

        try (Cursor cursor = cr.query(Crumbs.CONTENT_URI, Crumbs.PROJECTION_ALL,
                where, args, Crumbs.SORT_ORDER_DEFAULT)) {

            if (cursor == null)
                return null;

            JSONArray crumbsArray = new JSONArray();

            while (cursor.moveToNext()) {

                JSONObject attributes = new JSONObject();
                attributes.put("nature", cursor.getString(1));
                attributes.put("geolocation", String.format("SRID=4326; POINT(%s %s)", cursor.getDouble(3), cursor.getDouble(2)));
                attributes.put("read_at", ISO8601.format(new Date(cursor.getLong(4))));
                attributes.put("accuracy", cursor.getString(5));

                JSONObject hash = new JSONObject();
                Uri metadata = Uri.parse("http://domain.tld?" + cursor.getString(6));
                Set<String> keys = metadata.getQueryParameterNames();
                if (keys.size() > 0) {
                    for (String key : keys)
                        if (!key.equals("null"))
                            hash.put(key, metadata.getQueryParameter(key));
                    if (hash.length() > 0)
                        attributes.put("metadata", hash);
                }
                crumbsArray.put(attributes);
            }
            return crumbsArray;

        } catch (JSONException e) {
            Sentry.captureException(e);
            e.printStackTrace();
        }
        return null;
    }

    private void getInterventions() {

        setNotification("Synchro. des interventions");

        // TODO :: should be not necessary (rewrite insert)
        // Delete InterventionParameters to receive new ones
        cr.delete(InterventionParameters.CONTENT_URI, null, null);

        List<Intervention> interventionList = null;

        String attributes = String.format("?nature=request&user_email=%s&without_interventions=true", AccountTool.getEmail(account));

        try {
            interventionList = Intervention.all(instance, attributes);
        } catch (JSONException | IOException | HTTPException e) {
            Sentry.captureException(e);
            e.printStackTrace();
        }

        if (interventionList == null)
            return;

        // Create the lists containing items number
        List<Integer> remoteNumbers = new ArrayList<>();
        List<Integer> localNumbers = _getLocalItemsNumbers();

        for (Intervention intervention : interventionList) {

            // Adds current remote item number to the list
            remoteNumbers.add(intervention.getNumber());

            _insertIntervention(intervention);
            _insertInterventionParams(intervention);
        }

        // Check and delete local items if not existing on remote
        for (int number : localNumbers) {
            if (!remoteNumbers.contains(number)) {
                final String where = "user == ? AND number == ?";
                String[] args = new String[]{account.name, String.valueOf(number)};
                cr.delete(Interventions.CONTENT_URI, where, args);
            }
        }
    }

    private void _insertIntervention(Intervention intervention) {

        ContentValues cv = new ContentValues();
        cv.put(Interventions.EK_ID, intervention.getId());
        cv.put(Interventions.NAME, intervention.getName());
        cv.put(Interventions.TYPE, intervention.getType());
        cv.put(Interventions.PROCEDURE_NAME, intervention.getProcedureName());
        cv.put(Interventions.NUMBER, intervention.getNumber());
        cv.put(Interventions.STARTED_AT, toIso8601Android(intervention.getStartedAt()));
        cv.put(Interventions.STOPPED_AT, toIso8601Android(intervention.getStoppedAt()));
        cv.put(Interventions.DESCRIPTION, intervention.getDescription());
        cv.put(Interventions.USER, account.name);

        if (_idExists(intervention.getId())) {
            final String where = "ek_id == ?";
            String[] args = new String[]{String.valueOf(intervention.getId())};
            cr.update(Interventions.CONTENT_URI, cv, where, args);
        } else {
            cv.put(Interventions.UUID, UUID.randomUUID().toString());
            cr.insert(Interventions.CONTENT_URI, cv);
        }
    }

    private boolean _idExists(int id) {

        final String where = "user == ? AND ek_id == ?";
        String[] args = new String[]{account.name, String.valueOf(id)};

        try (Cursor curs = cr.query(Interventions.CONTENT_URI, Interventions.PROJECTION_NONE,
                where, args, null)) {

            return curs != null && curs.getCount() != 0;
        }
    }

    private void _insertInterventionParams(Intervention intervention) {

        int interventionId = _getInterventionId(intervention.getId());

        if (interventionId == 0)
            return;

        ContentValues cv = new ContentValues();
        int paramLength = intervention.getParamLength();
        int i = -1;

        while (++i < paramLength) {
            try {
                cv.clear();
                cv.put(InterventionParameters.EK_ID, intervention.getParamID(i));
                cv.put(InterventionParameters.NAME, intervention.getParamName(i));
                cv.put(InterventionParameters.FK_INTERVENTION, interventionId);
                cv.put(InterventionParameters.ROLE, intervention.getParamRole(i));
                cv.put(InterventionParameters.LABEL, intervention.getParamLabel(i));
                cv.put(InterventionParameters.PRODUCT_NAME, intervention.getProductName(i));
                cv.put(InterventionParameters.PRODUCT_ID, intervention.getProductID(i));
                if (intervention.getParamRole(i).equals("target")) {
                    String json = Intervention.getGeoJson(instance, intervention.getParamID(i));
                    if (json != null)
                        cv.put(InterventionParameters.SHAPE, json);
                }
                cr.insert(InterventionParameters.CONTENT_URI, cv);

            } catch (JSONException e) {
                Sentry.captureException(e);
                e.printStackTrace();
            }
        }
    }

    private int _getInterventionId(int refId) {

        final String where = "user == ? AND ek_id == ?";
        String[] args = new String[]{account.name, String.valueOf(refId)};

        try (Cursor cursor = cr.query(Interventions.CONTENT_URI, Interventions.PROJECTION_NONE,
                where, args, null)) {

            if (cursor != null && cursor.moveToFirst())
                return cursor.getInt(0);
            else
                return 0;
        }
    }

    private List<Integer> _getLocalItemsNumbers() {

        final String where = "user == ? AND type == 'request'";
        String[] args = new String[]{account.name};

        try (Cursor cursor = cr.query(Interventions.CONTENT_URI, Interventions.PROJECTION_NUMBER,
                where, args,null)) {

            List<Integer> localItemsNumbers = new ArrayList<>();

            while (cursor != null && cursor.moveToNext())
                localItemsNumbers.add(cursor.getInt(1));

            return localItemsNumbers;
        }
    }

    private void postObservations() {

        setNotification("Synchro. des observations");

        final String where = "user == ? AND synced == 0";
        String[] args = new String[]{account.name};

        // Get all non-synced observations
        try (Cursor cursor = cr.query(Observations.CONTENT_URI, Observations.PROJECTION_ALL,
                where, args, Observations.SORT_ORDER_DEFAULT)) {

            if (cursor == null)
                return;

            while (cursor.moveToNext())
                _postNewObservation(cursor);

        } catch (HTTPException | IOException | JSONException e) {
            Sentry.captureException(e);
            e.printStackTrace();
        }
    }

    private void _postNewObservation(Cursor obsCurs)
            throws JSONException, IOException, HTTPException {

        // Creates JSONObject for each observation
        JSONObject attributes = new JSONObject();
        attributes.put("observed_on", iso8601date.format(new Date(obsCurs.getLong(2))));
        attributes.put("activity_id", obsCurs.getInt(1));

        if (!obsCurs.isNull(4))
            attributes.put("scale_id", obsCurs.getInt(4));

        if (!obsCurs.isNull(6))
            attributes.put("description", obsCurs.getString(6));

        // Parse geolocation
        if (!obsCurs.isNull(7) && !obsCurs.isNull(8))
            attributes.put("geolocation", String.format("SRID=4326; POINT(%s %s)",
                    obsCurs.getDouble(7), obsCurs.getDouble(8)));

        // Attach pictures if available
        if (!obsCurs.isNull(5)) {
            JSONArray array = _attachPictures(obsCurs.getString(5).split(","));
            if (array.length() > 0)
                attributes.put("pictures", array);
        }

        // Parse plants
        if (!obsCurs.isNull(3)) {
            String[] plants = obsCurs.getString(3).split(",");
            JSONArray jsonArray = new JSONArray();
            for (String plant : plants) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("id", plant);
                jsonArray.put(jsonObject);
            }
            if (jsonArray.length() > 0)
                attributes.put("plants", jsonArray);
        }

        // Parse issues
        JSONArray issuesArray = _attacheObservationIssues(obsCurs.getInt(0));
        if (issuesArray.length() > 0)
            attributes.put("issues", issuesArray);

        // Send the request to API
        Observation.create(instance, attributes);

        // Marks them as synced
        ContentValues cv = new ContentValues();
        cv.put(Observations.SYNCED, 1);
        cr.update(Uri.withAppendedPath(Observations.CONTENT_URI,
                Long.toString(obsCurs.getLong(0))), cv, null, null);

    }

    private JSONArray _attacheObservationIssues(int observationId) {

        JSONArray jsonArray = new JSONArray();

        final String whereClause = "fk_observation == ?";
        String[] whereArgs = new String[]{String.valueOf(observationId)};

        // Get associated issues
        try (Cursor curs = cr.query(ObservationIssues.CONTENT_URI, ObservationIssues.PROJECTION_ALL,
                whereClause, whereArgs, null)) {

            if (curs == null)
                return jsonArray;

            while (curs.moveToNext()) {

                if (!curs.isNull(3)) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("id", curs.getInt(3));
                    jsonArray.put(jsonObject);
                }
            }
        } catch (JSONException e) {
            Sentry.captureException(e);
            e.printStackTrace();
        }
        return jsonArray;
    }

    private JSONArray _attachPictures(String[] paths) throws IOException {

        JSONArray jsonArray = new JSONArray();

        for (String path : paths) {

            Uri pictureUri = Uri.parse(path);
            String filePath = pictureUri.toString();

            if (Objects.equals(pictureUri.getScheme(), "content")) {

                String wholeID = DocumentsContract.getDocumentId(pictureUri);
                String id = wholeID.split(":")[1];
                String[] column = {MediaStore.Images.Media.DATA};
                String selection = MediaStore.Images.Media._ID + "==?";

                // TODO :: check if MediaStore.Images.Media.INTERNAL_CONTENT_URI is needed
                try (Cursor cursor = cr.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        column, selection, new String[]{id}, null)) {

                    if (cursor != null && cursor.moveToFirst())
                        filePath = "file://" + cursor.getString(0);
                }
            }

            // TODO :: do not crop image and rotate according to exif
            Uri uri = Uri.parse(filePath);
            InputStream is = cr.openInputStream(uri);
            Bitmap original = BitmapFactory.decodeStream(is);

            Bitmap rotated = ImageConverter.rotateImage(original, uri);
            Bitmap bitmap= ImageConverter.resizeImage(rotated, 2000);

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
            byte[] byteArrayImage = byteArrayOutputStream.toByteArray();
            String pictureJson = "data:image/jpeg;base64," + Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
            jsonArray.put(pictureJson);
        }

        return jsonArray;
    }

    private void getPlants() {

        setNotification("Synchro. des activités");

        // Check in database if there is plants, if not, do a full sync
        int count = (int) DatabaseUtils.queryNumEntries(db, "plants");
        String modifiedSinceParam = count > 0 ? lastSyncattribute : null;

        // Init empty list
        List<Plant> plantList = null;

        try {
            plantList = Plant.all(instance, modifiedSinceParam);
        } catch (JSONException | IOException | HTTPException | ParseException e) {
            Sentry.captureException(e);
            e.printStackTrace();
        }

        if (plantList == null)
            return;

        ArrayList<ContentProviderOperation> operations = new ArrayList<>();

        PROGRESS_MAX = plantList.size();
        PROGRESS_CURRENT = 0;

        updateNotificationProgress();

        for (Plant plants : plantList) {

            ContentValues cv = new ContentValues();
            cv.put(Plants.EK_ID, plants.getId());
            cv.put(Plants.NAME, plants.getName());
            cv.put(Plants.VARIETY, plants.getVariety());
            cv.put(Plants.ACTIVITY_ID, plants.getActivityID());
            cv.put(Plants.ACTIVITY_NAME, plants.getmActivityName());
            cv.put(Plants.ACTIVE, true);
            cv.put(Plants.USER, account.name);

            operations.add(ContentProviderOperation.newInsert(Plants.CONTENT_URI).withValues(cv).build());

            // Bulk insert
            if (++PROGRESS_CURRENT % BULK_SIZE == 0)
                bulkInsert(operations);
        }
        bulkInsert(operations);
    }

    private void getProducts() {

        // Define product types
        final String[] productTypes = new String[]{"equipment", "land_parcel", "matter", "plant", "worker", "building_division"};

        // Loop over product types
        for (String type : productTypes) {

            // Customise notification and reset to indeterminate
            setNotification(String.format("Synchro. des %ss", getTranslation(type).toLowerCase()));

            // Check in database if there is products, if not, do a full sync
            final String where = "variety == ? AND user == ?";
            String[] args = new String[]{type, accountURL};
            int count = (int) DatabaseUtils.queryNumEntries(db, "products", where, args);
            String modifiedSinceParam = count > 0 ? lastSyncattribute : null;

            // Force product type full sync from migration call
            if (prefs.getBoolean("force_sync_" + type, false))
                modifiedSinceParam = null;

            // Init empty list
            List<Product> productList = null;

            // Do the API call
            try {
                productList = Product.all(instance, modifiedSinceParam, type);
            } catch (JSONException |IOException | HTTPException e) {
                Sentry.captureException(e);
                e.printStackTrace();
            }

            // Continue the loop to next type if no products
            if (productList == null)
                continue;

            ArrayList<ContentProviderOperation> operations = new ArrayList<>();

            PROGRESS_MAX = productList.size();
            PROGRESS_CURRENT = 0;

            updateNotificationProgress();

            // Construct each product
            for (Product product : productList) {

                ContentValues cv = new ContentValues();
                cv.put(Products.EK_ID, product.id);
                cv.put(Products.NAME, product.name);
                cv.put(Products.SEARCH_NAME, StringUtils.stripAccents(product.name).toLowerCase());
                cv.put(Products.VARIETY, product.variety);
                cv.put(Products.ABILITIES, product.abilities);
                cv.put(Products.NUMBER, product.number);
                cv.put(Products.WORK_NUMBER, product.workNumber);
                cv.put(Products.POPULATION, product.population);
                cv.put(Products.UNIT, product.unit);
                cv.put(Products.CONTAINER_NAME, product.containerName);
                if (product.bornAt != null)
                    cv.put(Products.BORN_AT, product.bornAt.getTime());
                if (product.deadAt != null)
                    cv.put(Products.DEAD_AT, product.deadAt.getTime());
                cv.put(Products.NET_SURFACE_AREA, product.netSurfaceArea);
//                if (product.production_started_on != null)
//                    cv.put(Products.PRODUCTION_STARTED_ON, product.production_started_on.getTime());
//                if (product.production_stopped_on != null)
//                    cv.put(Products.PRODUCTION_STOPPED_ON, product.production_stopped_on.getTime());
                cv.put(Products.HAS_HOUR_COUNTER, product.hasHourCounter);
                cv.put(Products.REF_ID, product.refId);
                cv.put(Products.USER, accountURL);

                operations.add(ContentProviderOperation.newInsert(Products.CONTENT_URI).withValues(cv).build());

                // Bulk insert
                if (++PROGRESS_CURRENT % BULK_SIZE == 0)
                    bulkInsert(operations);
            }

            // Bulk insert lasting items
            bulkInsert(operations);

            // Remove force sync for current type if present
            if (prefs.getBoolean("force_sync_" + type, false))
                prefs.edit().remove("force_sync_" + type).apply();

        }
    }

    private void getVariants() {

        // Customise notification and reset to indeterminate
        setNotification("Synchro. des variantes");

        // Check in database if there is items, if not, do a full sync
        final String where = "user == ?";
        String [] args = new String[] {accountURL};
        long count = DatabaseUtils.queryNumEntries(db,"variants",where, args);
        String modifiedSinceParam = count > 0 ? lastSyncattribute : null;

        // Init empty list
        List<Variant> variantList = null;

        try {
            variantList = Variant.all(instance, modifiedSinceParam);
        } catch (JSONException | IOException | HTTPException e) {
            Sentry.captureException(e);
            e.printStackTrace();
        }

        // Finish if no variants
        if (variantList == null)
            return;

        ArrayList<ContentProviderOperation> operations = new ArrayList<>();

        PROGRESS_MAX = variantList.size();
        PROGRESS_CURRENT = 0;

        // Customise notification and reset
        updateNotificationProgress();

        for (Variant variant : variantList) {

            ContentValues cv = new ContentValues();
            cv.put(Variants.EK_ID, variant.id);
            cv.put(Variants.NAME, variant.name);
            cv.put(Variants.SEARCH_NAME, StringUtils.stripAccents(variant.name).toLowerCase());
            cv.put(Variants.VARIETY, variant.variety);
            cv.put(Variants.ABILITIES, variant.abilities);
            cv.put(Variants.NUMBER, variant.number);
            cv.put(Variants.USER, accountURL);

            operations.add(ContentProviderOperation.newInsert(Variants.CONTENT_URI).withValues(cv).build());

            // Bulk insert items
            if (++PROGRESS_CURRENT % BULK_SIZE == 0)
                bulkInsert(operations);
        }

        // Bulk insert last items
        bulkInsert(operations);
    }

    private void getLexiconPhytosanitaryReferences() {

        // Customise notification and reset to indeterminate
        setNotification("Synchro. des phytosanitaires");

        int count = (int) DatabaseUtils.queryNumEntries(db, "phytosanitary_references", null, null);

        // Init empty list
        List<PhytosanitaryReference> phytoList = null;

        try {
            if (count == 0)
                phytoList = PhytosanitaryReference.all(instance, null);
            else {
                JSONObject params = getCurrentChecksum(PhytosanitaryReferences.CONTENT_URI, PhytosanitaryReferences.PROJECTION_SYNC);
                phytoList = PhytosanitaryReference.update(instance, params);
            }

        } catch (JSONException | IOException | HTTPException e) {
            Sentry.captureMessage(e.getMessage());
            e.printStackTrace();
        }
        // Finish if no variants
        if (phytoList == null)
            return;

        ArrayList<ContentProviderOperation> operations = new ArrayList<>();

        PROGRESS_MAX = phytoList.size();
        PROGRESS_CURRENT = 0;

        // Customise notification and reset
        updateNotificationProgress();

        for (PhytosanitaryReference phyto : phytoList) {

            ContentValues cv = new ContentValues();
            cv.put(PhytosanitaryReferences.ID, phyto.id);
            cv.put(PhytosanitaryReferences.PRODUCT_TYPE, phyto.productType);
            cv.put(PhytosanitaryReferences.REFERENCE_NAME, phyto.referenceName);
            cv.put(PhytosanitaryReferences.NAME, phyto.name);
            cv.put(PhytosanitaryReferences.OTHER_NAME, phyto.otherName);
            cv.put(PhytosanitaryReferences.NATURE, phyto.nature);
            cv.put(PhytosanitaryReferences.ACTIVE_COMPOUNDS, phyto.activeCompounds);
            cv.put(PhytosanitaryReferences.MIX_CODE, phyto.mixCode);
            cv.put(PhytosanitaryReferences.FIELD_REENTRY_DELAY, phyto.fieldReentryDelay);
            cv.put(PhytosanitaryReferences.STATE, phyto.state);
            cv.put(PhytosanitaryReferences.STARTED_ON, phyto.startedOn);
            cv.put(PhytosanitaryReferences.STOPPED_ON, phyto.stoppedOn);
            cv.put(PhytosanitaryReferences.ALLOWED_MENTIONS, phyto.allowedMentions);
            cv.put(PhytosanitaryReferences.RESTRICTED_MENTIONS, phyto.restrictedMentions);
            cv.put(PhytosanitaryReferences.OPERATOR_PROTECTION, phyto.operatorProtection);
            cv.put(PhytosanitaryReferences.FIRM_NAME, phyto.firmName);
            cv.put(PhytosanitaryReferences.RECORD_CHECKSUM, phyto.recordChecksum);

            operations.add(ContentProviderOperation.newInsert(PhytosanitaryReferences.CONTENT_URI).withValues(cv).build());

            // Bulk insert items
            if (++PROGRESS_CURRENT % BULK_SIZE == 0)
                bulkInsert(operations);
        }

        // Bulk insert last items
        bulkInsert(operations);
    }

    private JSONObject getCurrentChecksum(Uri uri, String[] projection) throws JSONException {

        JSONObject json = new JSONObject();
        JSONArray array = new JSONArray();

        try (Cursor curs = cr.query(uri, projection,null,null,null)) {

            if (curs != null && curs.getCount() > 0) {
                while (curs.moveToNext()) {
                    JSONObject obj = new JSONObject();
                    obj.put("id", curs.getInt(0));
                    obj.put("record_checksum", curs.getString(1));
                    array.put(obj);
                }
            }
        }

        return json.put("data", array);
    }

    private void getLexiconPhytosanitaryUsages() {

        // Customise notification and reset to indeterminate
        setNotification("Synchro. des usages phyto.");

        try {

            Pair<JSONObject, List<PhytosanitaryUsage>> data = PhytosanitaryUsage.all(instance, "?paginate&per_page=1000");
            int page_count = data.first.getInt("page_count");
            PROGRESS_MAX = data.first.getInt("total_elements");
            PROGRESS_CURRENT = 0;
            _saveUsagesInDatabase(data.second);

            for (int i=2; i<=page_count; i++){
                data = PhytosanitaryUsage.all(instance, "?paginate&per_page=1000&page=" + i);
                _saveUsagesInDatabase(data.second);
            }

        } catch (JSONException | IOException | HTTPException e) {
            Sentry.captureMessage(e.getMessage());
            e.printStackTrace();
        }
    }

    private void _saveUsagesInDatabase(List<PhytosanitaryUsage> usagesList) {
        ArrayList<ContentProviderOperation> operations = new ArrayList<>();

        // Customise notification and reset
        updateNotificationProgress();

        for (PhytosanitaryUsage usage : usagesList) {

            ContentValues cv = new ContentValues();
            cv.put(PhytosanitaryUsages.ID, usage.id);
            cv.put(PhytosanitaryUsages.PRODUCT_ID, usage.productId);
            cv.put(PhytosanitaryUsages.EPHY_USAGE_PHRASE, usage.ephyUsagePhrase);
            cv.put(PhytosanitaryUsages.CROP, usage.crop);
            cv.put(PhytosanitaryUsages.SPECIES, usage.species);
            cv.put(PhytosanitaryUsages.TARGET_NAME, usage.targetName);
            cv.put(PhytosanitaryUsages.DESCRIPTION, usage.description);
            cv.put(PhytosanitaryUsages.TREATMENT, usage.treatment);
            cv.put(PhytosanitaryUsages.DOSE_QUANTITY, usage.doseQuantity);
            cv.put(PhytosanitaryUsages.DOSE_UNIT, usage.doseUnit);
            cv.put(PhytosanitaryUsages.DOSE_UNIT_NAME, usage.doseUnitName);
            cv.put(PhytosanitaryUsages.DOSE_UNIT_FACTOR, usage.doseUnitFactor);
            cv.put(PhytosanitaryUsages.HARVEST_DELAY_DAYS, usage.harvestDelayDays);
            cv.put(PhytosanitaryUsages.HARVEST_DELAY_BBCH, usage.harvestDelayBBCH);
            cv.put(PhytosanitaryUsages.APPLICATIONS_COUNT, usage.applicationsCount);
            cv.put(PhytosanitaryUsages.APPLICATIONS_FREQUENCY, usage.applicationsFrequency);
            cv.put(PhytosanitaryUsages.DEVELOPMENT_STAGE_MIN, usage.developmentStageMin);
            cv.put(PhytosanitaryUsages.DEVELOPMENT_STAGE_MAX, usage.developmentStageMax);
            cv.put(PhytosanitaryUsages.USAGE_CONDITIONS, usage.usageConditions);
            cv.put(PhytosanitaryUsages.AQUATIC_BUFFER, usage.aquaticBuffer);
            cv.put(PhytosanitaryUsages.ARTHROPOD_BUFFER, usage.arthropodBuffer);
            cv.put(PhytosanitaryUsages.PLANT_BUFFER, usage.plantBuffer);
            cv.put(PhytosanitaryUsages.DECISION_DATE, usage.decisionDate);
            cv.put(PhytosanitaryUsages.STATE, usage.state);
            cv.put(PhytosanitaryUsages.RECORD_CHECKSUM, usage.recordChecksum);

            operations.add(ContentProviderOperation.newInsert(PhytosanitaryUsages.CONTENT_URI).withValues(cv).build());

            // Bulk insert items
            if (++PROGRESS_CURRENT % BULK_SIZE == 0)
                bulkInsert(operations);
        }

        // Bulk insert last items
        bulkInsert(operations);
    }

    private void getEphyCropsets() {

        // Customise notification and reset to indeterminate
        setNotification("Synchro. des cropsets");

        // Init empty list
        List<EphyCropset> cropsets = null;

        try {
            cropsets = EphyCropset.all(instance, null);
        } catch (JSONException | IOException | HTTPException e) {
            Sentry.captureMessage(e.getMessage());
            e.printStackTrace();
        }
        // Finish if no variants
        if (cropsets == null)
            return;

        ArrayList<ContentProviderOperation> operations = new ArrayList<>();

        PROGRESS_MAX = cropsets.size();
        PROGRESS_CURRENT = 0;

        // Customise notification and reset
        updateNotificationProgress();

        for (EphyCropset cropset : cropsets) {

            ContentValues cv = new ContentValues();
            cv.put(EphyCropsets.ID, cropset.id);
            cv.put(EphyCropsets.NAME, cropset.name);
            cv.put(EphyCropsets.LABEL, cropset.label);
            cv.put(EphyCropsets.CROP_NAMES, cropset.cropNames);
            cv.put(EphyCropsets.CROP_LABELS, cropset.cropLabels);
            cv.put(EphyCropsets.RECORD_CHECKSUM, cropset.recordChecksum);

            operations.add(ContentProviderOperation.newInsert(EphyCropsets.CONTENT_URI).withValues(cv).build());

            // Bulk insert items
            if (++PROGRESS_CURRENT % BULK_SIZE == 0)
                bulkInsert(operations);
        }

        // Bulk insert last items
        bulkInsert(operations);
    }

    private void postDetailedInterventions() {

        setNotification("Synchronisation des nouvelles interventions");

        // Get interventions that are not yet created in Ekylibre (no ek_id)
        String where = "user == ? AND ek_id IS NULL AND status == ?";
        if (retryFailed)
            where = "user == ? AND (ek_id IS NULL OR ek_id == -1) AND status == ?";
        final String[] args = new String[] {accountURL, FINISHED};

        // Get all non-synced observations
        try (Cursor cursor = cr.query(DetailedInterventions.CONTENT_URI,
                DetailedInterventions.PROJECTION_ALL, where, args,
                DetailedInterventions.ORDER_BY_ID_DESC)) {

            if (cursor == null)
                return;

            int _idxId = cursor.getColumnIndex(DetailedInterventions._ID);
            int _idxProcedureName = cursor.getColumnIndex(DetailedInterventions.PROCEDURE_NAME);
            int _idxActions = cursor.getColumnIndex(DetailedInterventions.ACTIONS);
            int _idxDesc = cursor.getColumnIndex(DetailedInterventions.DESCRIPTION);


            // Loop over all selected interventions
            while (cursor.moveToNext()) {

                int _id = cursor.getInt(_idxId);

                Log.i(TAG, "Current inter id -> " + _id);

                // Create new DetailedIntervention payload
                JSONObject payload = new JSONObject();
                JSONObject provider = new JSONObject();
                provider.put("zero_id", _id);
                payload.put("providers", provider);
                payload.put("procedure_name", cursor.getString(_idxProcedureName));

                if (!cursor.isNull(_idxActions)) {
                    JSONArray array = new JSONArray(cursor.getString(_idxActions).split(","));
                    payload.put("actions", array);
                }

                if (!cursor.isNull(_idxDesc)) {
                    payload.put("description", cursor.getString(_idxDesc));
                }

                // Prepare InterventionAttributes query
                final String whereId = "detailed_intervention_id == ?";
                final String whereIdAndNatureIsNull = "detailed_intervention_id == ? AND nature IS NULL";
                final String[] id = new String[] {String.valueOf(_id)};  // The current id

                // --------------- //
                // Working periods //
                // --------------- //

                try (Cursor cursPeriod = cr.query(WorkingPeriodAttributes.CONTENT_URI,
                        WorkingPeriodAttributes.PROJECTION_ALL, whereIdAndNatureIsNull, id, null)) {

                    // Create JSON array for working periods
                    JSONArray workingPeriods = new JSONArray();

                    while (cursPeriod != null && cursPeriod.moveToNext()) {
                        JSONObject period = new JSONObject();
//                        period.put("started_at", simpleISO8601.format(new Date(cursPeriod.getLong(0))));
//                        period.put("stopped_at", simpleISO8601.format(new Date(cursPeriod.getLong(1))));
                        period.put("started_at", cursPeriod.getString(0));
                        period.put("stopped_at", cursPeriod.getString(1));
                        workingPeriods.put(period);
                    }

                    // Add working periods to main payload
                    payload.put("working_periods_attributes", workingPeriods);
                }

                // --------------------- //
                // Group zone attributes //
                // --------------------- //

                JSONArray zoneArray = new JSONArray();
                try (Cursor cursZone = cr.query(GroupZones.CONTENT_URI,
                        GroupZones.PROJECTION_ALL, whereId, id, null)) {

                    while (cursZone != null && cursZone.moveToNext()) {
                        JSONObject zone = new JSONObject();
                        zone.put("reference_name", "zone");

                        // Get target
                        try (Cursor targetCurs = cr.query(DetailedInterventionAttributes.CONTENT_URI,
                                DetailedInterventionAttributes.PROJECTION_ALL, "_id == ?",
                                new String[] {cursZone.getString(1)}, null, null)) {

                            JSONArray targetObjectList = new JSONArray();

                            while (targetCurs != null && targetCurs.moveToNext()) {
                                targetObjectList.put(_composeJSONObject(targetCurs));
                                zone.put("targets_attributes", targetObjectList);
                            }
                        }

                        // Get output
                        try (Cursor outputCurs = cr.query(DetailedInterventionAttributes.CONTENT_URI,
                                DetailedInterventionAttributes.PROJECTION_ALL, "_id == ?",
                                new String[] {cursZone.getString(2)}, null, null)) {

                            JSONArray outputObjectList = new JSONArray();

                            while (outputCurs != null && outputCurs.moveToNext()) {
                                JSONObject outputObject = new JSONObject();
                                outputObject.put("variant_id", outputCurs.getInt(1));
                                outputObject.put("reference_name", outputCurs.getString(2));
                                outputObject.put("variety", cursZone.getString(3));
                                outputObject.put("batch_number", cursZone.getString(4));
                                outputObjectList.put(outputObject);
                                zone.put("outputs_attributes", outputObjectList);
                            }
                        }
                        // Add created zone to list
                        zoneArray.put(zone);
                    }
                }

                // --------------------- //
                // Group work attributes //
                // --------------------- //

                JSONArray workArray = new JSONArray();
                try (Cursor cursWork = cr.query(GroupWorks.CONTENT_URI,
                        GroupWorks.PROJECTION_ALL, whereId, id, null)) {

                    while (cursWork != null && cursWork.moveToNext()) {
                        JSONObject work = new JSONObject();
                        work.put("reference_name", "work");

                        // Get target
                        try (Cursor targetCurs = cr.query(DetailedInterventionAttributes.CONTENT_URI,
                                DetailedInterventionAttributes.PROJECTION_ALL, "_id == ?",
                                new String[] {cursWork.getString(1)}, null)) {

                            JSONArray targetObjectList = new JSONArray();

                            while (targetCurs != null && targetCurs.moveToNext()) {
                                targetObjectList.put(_composeJSONObject(targetCurs));
                                work.put("targets_attributes", targetObjectList);
                            }
                        }

                        // Get inputs
                        String[] inputIds = cursWork.getString(2).split(",");
                        JSONArray inputObjectList = new JSONArray();
                        for (String inputId : inputIds) {
                            try (Cursor inputCurs = cr.query(DetailedInterventionAttributes.CONTENT_URI,
                                    DetailedInterventionAttributes.PROJECTION_ALL, "_id == ?",
                                    new String[]{inputId}, null)) {

                                while (inputCurs != null && inputCurs.moveToNext()) {
                                    inputObjectList.put(_composeQuantityJSONObject(inputCurs, "product_id"));
                                }
                            }
                        }
                        // Add all inputs
                        work.put("inputs_attributes", inputObjectList);

                        // Add created zone to list
                        workArray.put(work);
                    }
                }

                // ----------------------- //
                // Intervention attributes //
                // ----------------------- //
                try (Cursor cursAttrs = cr.query(DetailedInterventionAttributes.CONTENT_URI,
                        DetailedInterventionAttributes.PROJECTION_ALL, whereId, id, null)) {

                    if (cursAttrs == null)
                        return;

                    JSONArray workersArray = new JSONArray();
                    JSONArray targetsArray = new JSONArray();
                    JSONArray equipmentsArray = new JSONArray();
                    JSONArray inputsArray = new JSONArray();
                    JSONArray outputsArray = new JSONArray();

                    // Get index of fields in cursor
                    int idxRole = cursAttrs.getColumnIndex(DetailedInterventionAttributes.ROLE);
                    int idxGroupId = cursAttrs.getColumnIndex(DetailedInterventionAttributes.GROUP_ID);

                    while (cursAttrs.moveToNext()) {
                        String role = cursAttrs.getString(idxRole);
                        switch (role) {

                            case "workers":
                                workersArray.put(_composeJSONObject(cursAttrs));
                                break;

                            case "equipments":
                                equipmentsArray.put(_composeJSONObject(cursAttrs));
                                break;

                            case "targets":
                                if (cursAttrs.isNull(idxGroupId))
                                    targetsArray.put(_composeJSONObject(cursAttrs));
                                break;

                            case "inputs":
                                if (cursAttrs.isNull(idxGroupId))
                                    inputsArray.put(_composeQuantityJSONObject(cursAttrs, "product_id"));
                                break;

                            case "outputs":
                                if (cursAttrs.isNull(idxGroupId))
                                    outputsArray.put(_composeQuantityJSONObject(cursAttrs, "variant_id"));
                                break;
                        }
                    }

                    // Add all attributs to corresponding payload value
                    if (workersArray.length() > 0)
                        payload.put("doers_attributes", workersArray);

                    if (equipmentsArray.length() > 0)
                        payload.put("tools_attributes", equipmentsArray);

                    if (targetsArray.length() > 0)
                        payload.put("targets_attributes", targetsArray);

                    if (inputsArray.length() > 0)
                        payload.put("inputs_attributes", inputsArray);

                    if (outputsArray.length() > 0)
                        payload.put("outputs_attributes", outputsArray);

                    if (zoneArray.length() > 0)
                        payload.put("group_parameters_attributes", zoneArray);
                    else if (workArray.length() > 0)
                        payload.put("group_parameters_attributes", workArray);
                }

                long resultId = -1L;
                String errors = null;
                try {
                    // Push the new DetailedIntervention
                    JSONObject jsonResult = DetailedIntervention.create(instance, payload);

                    if (jsonResult.has("id") && jsonResult.getLong("id") != -1) {
                        resultId = jsonResult.getLong("id");
                    } else {
                        errors = jsonResult.getString("errors");
                        if (errors == null) errors = jsonResult.getString("error");
                        hasErrors = true;
                        Sentry.setExtra("payload", payload.toString());
                        Sentry.captureMessage(errors);

                        return;
                    }

                } catch (JSONException | IOException | HTTPException e) {
                    Sentry.captureException(e);
                    e.printStackTrace();
                }

                if (BuildConfig.DEBUG )
                    if (errors != null)
                        Log.e(TAG, "[detailed_intervention] -> " + errors);
                    else
                        Log.i(TAG, "[detailed_intervention] with id #" + resultId + " created !");

                // Save the returning id
                ContentValues cv = new ContentValues();
                cv.put(DetailedInterventions.EK_ID, resultId);
                if (errors != null)
                    cv.put(DetailedInterventions.ERROR, errors);
                Uri uri = Uri.withAppendedPath(DetailedInterventions.CONTENT_URI, cursor.getString(_idxId));
                cr.update(uri, cv, null, null);

                // Send intervention participations if existing
                try {
                    _postInterventionParticipations(_id, resultId);
                } catch (JSONException | IOException | HTTPException e) {
                    Sentry.captureException(e);
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            Sentry.captureException(e);
            e.printStackTrace();
        }
    }

    private void _postInterventionParticipations(int interId, long createdId)
            throws JSONException, IOException, HTTPException {

        final String whereIdAndNatureNotNull = "detailed_intervention_id == ? AND nature IS NOT NULL";
        final String[] id = new String[] {String.valueOf(interId)};  // The current intervention id

        try (Cursor cursPeriod = cr.query(WorkingPeriodAttributes.CONTENT_URI,
                WorkingPeriodAttributes.PROJECTION_ALL, whereIdAndNatureNotNull, id, null)) {

            JSONArray workingPeriods = new JSONArray();

            while (cursPeriod != null && cursPeriod.moveToNext()) {
                JSONObject period = new JSONObject();
                period.put("started_at", cursPeriod.getString(0));
                period.put("stopped_at", cursPeriod.getString(1));
                period.put("nature", cursPeriod.getString(2));
                workingPeriods.put(period);
            }

//            if (workingPeriods.length() > 0) {

                // If there is workingPeriods, there is a regular Intervention associated, so update it
                _updateInterventionState(interId);

                JSONObject payload = new JSONObject();
                payload.put("state", "done");
                payload.put("intervention_id", createdId);
                payload.put("working_periods", workingPeriods);

                // Push the new DetailedIntervention
                Intervention.create(instance, payload);
//            }
        }
    }

    private void _updateInterventionState(int detailedId) {

        final String where = "detailed_intervention_id == ?";
        final String[] args = new String[]{String.valueOf(detailedId)};

        try (Cursor curs = cr.query(Interventions.CONTENT_URI,
                Interventions.PROJECTION_NONE, where, args, null)) {

            while (curs != null && curs.moveToNext()) {

                // Set the regular intervention as synced
                ContentValues cv = new ContentValues();
                cv.put(Interventions.STATE, SYNCED);
                Uri uri = Uri.withAppendedPath(Interventions.CONTENT_URI, curs.getString(0));
                cr.update(uri, cv, null, null);
            }
        }
    }

    private JSONObject _composeJSONObject(Cursor curs) throws JSONException {

        // Get index of fields in cursor
        int idxProductId = curs.getColumnIndex(DetailedInterventionAttributes.REFERENCE_ID);
        int idxReferenceName = curs.getColumnIndex(DetailedInterventionAttributes.REFERENCE_NAME);
        int idxHourMeter = curs.getColumnIndex(DetailedInterventionAttributes.HOUR_METER);

        // Build the JSON object
        JSONObject obj = new JSONObject();
        obj.put("product_id", curs.getInt(idxProductId));
        obj.put("reference_name", curs.getString(idxReferenceName));
        if (!curs.isNull(idxHourMeter) && Float.parseFloat(curs.getString(idxHourMeter)) > 0) {
            JSONObject readingsAttributes = new JSONObject(); //
            readingsAttributes.put("indicator_name", "hour_counter");
            readingsAttributes.put("measure_value_value", curs.getString(idxHourMeter));
            readingsAttributes.put("measure_value_unit", "hour");
            obj.put("readings_attributes", readingsAttributes);
        }
        return obj;
    }

    private JSONObject _composeQuantityJSONObject(Cursor curs, String idType) throws JSONException {

        // Get index of fields in cursor
        int idxProductId = curs.getColumnIndex(DetailedInterventionAttributes.REFERENCE_ID);
        int idxReferenceName = curs.getColumnIndex(DetailedInterventionAttributes.REFERENCE_NAME);
        int idxQuantityValue = curs.getColumnIndex(DetailedInterventionAttributes.QUANTITY_VALUE);
        int idxQuantityIndicator = curs.getColumnIndex(DetailedInterventionAttributes.QUANTITY_INDICATOR);
        int idxQuantityName = curs.getColumnIndex(DetailedInterventionAttributes.QUANTITY_NAME);
        int idxQuantityUnit = curs.getColumnIndex(DetailedInterventionAttributes.UNIT_NAME);

        JSONObject obj = new JSONObject();
        obj.put(idType, curs.getInt(idxProductId));
        obj.put("reference_name", curs.getString(idxReferenceName));
        if (!curs.isNull(idxQuantityValue))
            obj.put("quantity_value", curs.getDouble(idxQuantityValue));
        if (!curs.isNull(idxQuantityName))
            obj.put("quantity_handler", curs.getString(idxQuantityName));
        else
            obj.put("quantity_handler", curs.getString(idxQuantityIndicator));
        if (!curs.isNull(8))
            obj.put("usage_id", curs.getString(8));
        return obj;
    }

    private String getLastSyncAttribute() {
        String attribute = null;
        long lastSyncDate = prefs.getLong("last_sync_date_" + account.name, 0);
        if (lastSyncDate > 0) {
            try {
                attribute = "?modified_since=" + URLEncoder.encode(
                        ISO8601.format(new Date(lastSyncDate)), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                Sentry.captureException(e);
                e.printStackTrace();
            }
        }
        Log.d(TAG, "getLastSyncAttribute() -> " + attribute);
        return attribute;
    }

    private int getWorkerId() {
        SharedPreferences.Editor editor = prefs.edit();
        String email = AccountTool.getEmail(account);
        try {
            JSONObject response = instance.getJSONObject("/api/v1/profile", "");
            if (response.has("worker_id")) {
                int id = response.getInt("worker_id");
                editor.putInt(email, id).apply();
                Log.d(TAG, "worker id -> " + id);
                return id;
            } else {
                Log.d(TAG, "no worker id");
                editor.remove(email).apply();
            }
        } catch (JSONException | IOException | HTTPException e) {
            editor.remove(email).apply();
            Sentry.captureException(e);
            e.printStackTrace();
        }
        return -1;
    }

    private void setNotification(String title) {
        Log.d(TAG, title);
        builder.setContentTitle(title)
                .setContentText("en attente du serveur")
                .setProgress(0, 0, true);
        notificationManager.notify(NOTIFICATION_ID, builder.build());
    }

    private void updateNotificationProgress() {
        builder.setContentText(PROGRESS_CURRENT + " sur " + PROGRESS_MAX)
                .setProgress(PROGRESS_MAX, PROGRESS_CURRENT, false);
        notificationManager.notify(NOTIFICATION_ID, builder.build());
    }

    private void bulkInsert(ArrayList<ContentProviderOperation> ops) {
        updateNotificationProgress();
        try {
            cr.applyBatch(AUTHORITY, ops);
        } catch (OperationApplicationException | RemoteException e) {
            Sentry.captureException(e);
            e.printStackTrace();
        }
        ops.clear();
    }

    private Instance getInstance() {
        Instance inst = null;
        try {
            inst = new Instance(account, AccountManager.get(this));
        } catch (AccountsException | IOException e) {
            Sentry.captureException(e);
            e.printStackTrace();
        }
        return inst;
    }

    private void postponeSync(int minutes) {
        Intent intent = new Intent(this, AlarmReceiver.class).setAction("SYNC_ALARM");
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                this, 1010, intent, PendingIntent.FLAG_ONE_SHOT);

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        if (alarmManager != null)
            alarmManager.set(AlarmManager.RTC_WAKEUP,
                    DateUtils.addMinutes(new Date(), minutes).getTime(), pendingIntent);
    }
}
