package ekylibre.service;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class LongSyncResultReceiver extends ResultReceiver {

    private SyncReceiver syncReceiver;

    public LongSyncResultReceiver(Handler handler, SyncReceiver receiver) {
        super(handler);
        syncReceiver = receiver;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if (syncReceiver != null) {
            /*
             * We pass the resulting data from the service to the activity
             * using the SyncReceiver interface
             */
            syncReceiver.onReceiveResult(resultCode, resultData);
        }
    }


    public interface SyncReceiver {
        public void onReceiveResult(int resultCode, Bundle resultData);
    }
}
