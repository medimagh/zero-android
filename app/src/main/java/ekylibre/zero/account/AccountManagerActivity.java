package ekylibre.zero.account;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;

import ekylibre.util.UpdatableActivity;
import ekylibre.zero.AuthenticatorActivity;
import ekylibre.zero.R;
//import ekylibre.APICaller.SyncAdapter;
import ekylibre.util.AccountTool;

import static ekylibre.zero.home.Zero.ACCOUNT_TYPE;

public class AccountManagerActivity extends UpdatableActivity
{
    private final static String TAG = "AccountManager";
    public final static String CURRENT_ACCOUNT_NAME = "Current account";
    public final static String CURRENT_ACCOUNT_INSTANCE = "Current instance";

    @Override
    public void onStart() {
        super.onStart();
        if (!AccountTool.isAnyAccountExist(this))
            AccountTool.askForAccount(this, this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_manager);
        super.setToolBar();
        setTitle(R.string.accountManager);

        ListView accountListView = findViewById(R.id.account_list);
        setAccountList(accountListView);
    }

    /*
    ** Set the listView with ekylibre accounts
    ** Click on account make it the current account
    */
    private void setAccountList(ListView accountListView) {
        Account[] arrayAccount = AccountManager.get(this).getAccountsByType(ACCOUNT_TYPE);

        ArrayList<Account> listAccount = new ArrayList<>(Arrays.asList(arrayAccount));
        AccountAdapter accountAdapter = new AccountAdapter(this, listAccount, this);
        accountListView.setAdapter(accountAdapter);

        accountListView.setOnItemClickListener((parent, view, position, id) -> {
            Account newCurrAccount = (Account) parent.getItemAtPosition(position);
//            SharedPreferences preferences = getSharedPreferences(getPackageName() + "_preferences", Context.MODE_MULTI_PROCESS);
//            preferences.edit().putString(CURRENT_ACCOUNT_NAME, newCurrAccount.name)
//                    .putBoolean("accountChanged", true).apply();
            Log.d(TAG, "New current account is ==> " + newCurrAccount.name);
//            syncAll(newCurrAccount);

            finish();
        });
    }

//    protected void syncAll(Account account) {
//        if (account == null)
//            return ;
//        Log.d(TAG, "syncAll: " + account.toString() + ", " + ZeroContract.AUTHORITY);
//        Bundle extras = new Bundle();
//        extras.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
//        extras.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
//        ContentResolver.requestSync(account, ZeroContract.AUTHORITY, extras);
//
////        Toast toast = Toast.makeText(getApplicationContext(), R.string.data_synced, Toast.LENGTH_SHORT);
////        toast.show();
//    }

    public void addAccount(View v) {
        Intent intent = new Intent(this, AuthenticatorActivity.class);
        intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, ACCOUNT_TYPE);
        intent.putExtra(AuthenticatorActivity.KEY_REDIRECT, AuthenticatorActivity.CHOICE_REDIRECT_TRACKING);
        startActivity(intent);
        finish();
    }

}