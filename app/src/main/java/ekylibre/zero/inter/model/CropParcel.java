package ekylibre.zero.inter.model;

public class CropParcel {

    public String name;
    public boolean isSelected;

    public CropParcel(String name, boolean isSelected) {
        this.name = name;
        this.isSelected = isSelected;
    }

}
