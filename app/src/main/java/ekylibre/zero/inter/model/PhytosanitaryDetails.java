package ekylibre.zero.inter.model;

import android.database.Cursor;

import java.util.List;

import ekylibre.database.ZeroContract.PhytosanitaryReferences;

public class PhytosanitaryDetails {

    public int id;
    public String productType;
    public String referenceName;
    public String name;
    public String otherName;
    public String nature;
    public String activeCompounds;
    public int mixCode;
    public Integer fieldReentryDelay;
    public String state;
    public String startedOn;
    public String stoppedOn;
    public String allowedMentions;
    public String restrictedMentions;
    public String operatorProtection;
    public String firmName;
    public List<PhytosanitaryUsage> usages;

    public PhytosanitaryDetails(int id, String productType, String referenceName, String name, String otherName, String nature, String activeCompounds, int mixCode, Integer fieldReentryDelay, String state, String startedOn, String stoppedOn, String allowedMentions, String restrictedMentions, String operatorProtection, String firmName, List<PhytosanitaryUsage> usages) {
        this.id = id;
        this.productType = productType;
        this.referenceName = referenceName;
        this.name = name;
        this.otherName = otherName;
        this.nature = nature;
        this.activeCompounds = activeCompounds;
        this.mixCode = mixCode;
        this.fieldReentryDelay = fieldReentryDelay;
        this.state = state;
        this.startedOn = startedOn;
        this.stoppedOn = stoppedOn;
        this.allowedMentions = allowedMentions;
        this.restrictedMentions = restrictedMentions;
        this.operatorProtection = operatorProtection;
        this.firmName = firmName;
    }

    private PhytosanitaryDetails() { }

    public static PhytosanitaryDetails getPhytosanitaryDetails(Cursor curs) {

        PhytosanitaryDetails phyto =  new PhytosanitaryDetails();

        phyto.id = curs.getInt(curs.getColumnIndex(PhytosanitaryReferences.ID));
        phyto.productType = curs.getString(curs.getColumnIndex(PhytosanitaryReferences.PRODUCT_TYPE));
        phyto.referenceName = curs.getString(curs.getColumnIndex(PhytosanitaryReferences.REFERENCE_NAME));
        phyto.name = curs.getString(curs.getColumnIndex(PhytosanitaryReferences.NAME));
        phyto.otherName = curs.getString(curs.getColumnIndex(PhytosanitaryReferences.OTHER_NAME));
        phyto.nature = curs.getString(curs.getColumnIndex(PhytosanitaryReferences.NATURE));
        phyto.activeCompounds = curs.getString(curs.getColumnIndex(PhytosanitaryReferences.ACTIVE_COMPOUNDS));
        phyto.mixCode = curs.getInt(curs.getColumnIndex(PhytosanitaryReferences.MIX_CODE));
        phyto.fieldReentryDelay = curs.getInt(curs.getColumnIndex(PhytosanitaryReferences.FIELD_REENTRY_DELAY));
        phyto.state = curs.getString(curs.getColumnIndex(PhytosanitaryReferences.STATE));
        phyto.startedOn = curs.getString(curs.getColumnIndex(PhytosanitaryReferences.STARTED_ON));
        phyto.stoppedOn = curs.getString(curs.getColumnIndex(PhytosanitaryReferences.STOPPED_ON));
        phyto.allowedMentions = curs.getString(curs.getColumnIndex(PhytosanitaryReferences.ALLOWED_MENTIONS));
        phyto.restrictedMentions = curs.getString(curs.getColumnIndex(PhytosanitaryReferences.RESTRICTED_MENTIONS));
        phyto.operatorProtection = curs.getString(curs.getColumnIndex(PhytosanitaryReferences.OPERATOR_PROTECTION));
        phyto.firmName = curs.getString(curs.getColumnIndex(PhytosanitaryReferences.FIRM_NAME));

        return phyto;
    }
}
