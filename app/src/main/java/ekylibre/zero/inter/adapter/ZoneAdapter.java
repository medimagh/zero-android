package ekylibre.zero.inter.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.Group;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import ekylibre.zero.R;
import ekylibre.zero.inter.fragment.InterventionFormFragment.OnFragmentInteractionListener;
import ekylibre.zero.inter.model.Zone;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;


public class ZoneAdapter extends RecyclerView.Adapter<ZoneAdapter.ViewHolder> {

    private final List<Zone> dataset;
    private OnFragmentInteractionListener listener;


    public ZoneAdapter(List<Zone> dataset, OnFragmentInteractionListener listener) {
        this.dataset = dataset;
        this.listener = listener;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        // Layout
        @BindView(R.id.parcel_add) MaterialButton parcelButton;
        @BindView(R.id.crop_add) MaterialButton cropButton;
        @BindView(R.id.variety_value) EditText varietyEditText;
        @BindView(R.id.batch_number_value) EditText batchNumberEditText;
        @BindView(R.id.parcel_chip) Chip parcelChip;
        @BindView(R.id.parcel_chip_group) ChipGroup parcelChipGroup;
        @BindView(R.id.crop_chip) Chip cropChip;
        @BindView(R.id.crop_chip_group) ChipGroup cropChipGroup;
        @BindView(R.id.zone_delete) ImageView deleteButton;
        @BindView(R.id.group) Group group;

        @OnClick(R.id.zone_delete)
        void deleteZoneItem() {
            if (getItemCount() == 1) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("L'intervention doit comporter une zone, vous ne pouvez pas la supprimer.");
                builder.setPositiveButton("Ok", (dialog, i) -> dialog.cancel());
                AlertDialog dialog = builder.create();
                dialog.show();
            } else {
                int index = dataset.indexOf(zone);
                dataset.remove(index);
                notifyItemRemoved(index);
            }
        }

        @OnClick(R.id.parcel_add)
        void onAddParcel() {
            listener.onFormFragmentInteraction(
                    "zone_land_parcel",
                    "is land_parcel",
                    String.valueOf(getAdapterPosition()));
        }

        @OnClick(R.id.crop_add)
        void onAddCrop() {
            listener.onFormFragmentInteraction(
                    "zone_plant",
                    "is plant",
                    String.valueOf(getAdapterPosition()));
        }

        @OnTextChanged(R.id.variety_value)
        void onVarietyChanged(CharSequence value) {
            zone.newName = value.toString();
        }

        @OnTextChanged(R.id.batch_number_value)
        void onBatchNumberChanged(CharSequence value) {
            zone.batchNumber = value.toString();
        }

        // Reference to the current zone
        Zone zone;
        Context context;

        ViewHolder(View itemView) {
            super(itemView);
            // Bind view to ButterKnife
            ButterKnife.bind(this, itemView);
            context = itemView.getContext();
        }
        
        /**
         * The methode in charge of displaying an item
         * @param currentZone The current Zone item
         */
        void display(Zone currentZone) {

            // Save reference of the current item
            this.zone = currentZone;

            // Hide delete button if just one item
//            deleteButton.setVisibility(getItemCount() > 1 ? VISIBLE : GONE);

            // Set Chips and fields visibility
            if (zone.landParcel != null) {
                parcelChip.setText(zone.landParcel.name);
                parcelChip.setOnCloseIconClickListener(view -> {
                    zone.landParcel = null;
                    parcelChipGroup.setVisibility(GONE);
                    parcelButton.setText(context.getString(R.string.add));
                });
                parcelChipGroup.setVisibility(VISIBLE);
                parcelButton.setText(R.string.modify);
            } else {
                parcelChipGroup.setVisibility(GONE);
                parcelButton.setText(context.getString(R.string.add));
            }

            int visibility = zone.plant != null ? VISIBLE : GONE;
            if (visibility == VISIBLE) {
                cropChip.setText(zone.plant.name);
                cropChip.setOnCloseIconClickListener(view -> {
                    zone.plant = null;
                    cropChipGroup.setVisibility(GONE);
                    group.setVisibility(GONE);
                    cropButton.setText(context.getString(R.string.add));
                });
                cropButton.setText(R.string.modify);
                varietyEditText.setText(zone.newName);
                batchNumberEditText.setText(zone.batchNumber);
            } else {
                cropButton.setText(context.getString(R.string.add));
            }
            cropChipGroup.setVisibility(visibility);
            group.setVisibility(visibility);
        }
    }

    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_group_zone, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.display(dataset.get(position));
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }
}
