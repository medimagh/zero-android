package ekylibre.zero.inter.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ekylibre.zero.R;
import ekylibre.zero.inter.fragment.SimpleChoiceFragment.OnFragmentInteractionListener;
import ekylibre.zero.inter.model.GenericItem;
import ekylibre.zero.inter.model.Work;
import ekylibre.zero.inter.model.Zone;

public class SimpleAdapter extends RecyclerView.Adapter<SimpleAdapter.ViewHolder> {

    private final static String TAG = "SimpleAdapter";
    private final List<GenericItem> dataset;
    private final OnFragmentInteractionListener listener;
    private String filter;
    private Object group;

    public SimpleAdapter(List<GenericItem> dataset, OnFragmentInteractionListener listener,
                         Object group, String filter) {
        this.dataset = dataset;
        this.listener = listener;
        this.filter = filter;
        this.group = group;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        GenericItem item;

        @BindView(R.id.item_first_line)
        TextView firstLine;
        @BindView(R.id.item_second_line)
        TextView secondLine;
        @BindView(R.id.item_third_line)
        TextView thirdLine;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void display(int position) {

            // Save item reference
            item = dataset.get(position);

            // Set background odd and even
            @ColorRes int colorId = position %2 == 1 ? R.color.another_light_grey : R.color.white;
            itemView.setBackgroundColor(ContextCompat.getColor(itemView.getContext(), colorId));

            // Set text
            firstLine.setText(item.name);

            // Compute second line
            StringBuilder sb = new StringBuilder();
            if (item.netSurfaceArea != null)
                sb.append(item.netSurfaceArea);
            else if (item.workNumber != null)
                sb.append(item.workNumber);
            if (item.number != null) {
                if (sb.length() > 0)
                    sb.append(" - ");
                sb.append(item.number);
            }
            if (item.containerName != null) {
                if (sb.length() > 0)
                    sb.append(" - ");
                sb.append(item.containerName);
            }
            secondLine.setText(sb.toString());

            // Set third line if needed
            if (item.population != null) {
                thirdLine.setText(item.population);
                thirdLine.setVisibility(View.VISIBLE);
            } else
                thirdLine.setVisibility(View.GONE);
        }

        @OnClick
        public void onClick(View view) {
            if (getAdapterPosition() == RecyclerView.NO_POSITION)
                return;
//            clearSelection();
//            Log.e(TAG, "Grouptype = " + groupType);
//            Log.e(TAG, "item ref = " + item.id);
            switch (filter) {
                case "is plant":
                    item.referenceName.put("zone", "outputs");
                    ((Zone) group).plant = item;
                    break;
                case "is land_parcel":
                    item.referenceName.put("zone", "targets");
                    ((Zone) group).landParcel = item;
                    break;
                case "is equipment":
                    item.referenceName.put("work", "targets");
                    ((Work) group).equipment = item;
                    break;
            }
            // Send back item to activity
            view.postDelayed(listener::onItemChoosed, 200);
            // listener.onItemChoosed(item, fragmentTag);

        }
    }

//    private void clearSelection() {
//        for (GenericItem item : dataset)
//            if (item.referenceName.containsKey(groupType)
//                    && item.referenceName.containsValue("targets")) {
//                item.referenceName.remove(groupType);
//                break;
//            }
//    }

    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_generic, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.display(position);
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }
}
