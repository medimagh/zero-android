package ekylibre.zero.inter;

import android.accounts.Account;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.util.Pair;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.snackbar.Snackbar;

import org.apache.commons.lang3.time.DateUtils;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import ekylibre.database.ZeroContract.DetailedInterventionAttributes;
import ekylibre.database.ZeroContract.DetailedInterventions;
import ekylibre.database.ZeroContract.GroupWorks;
import ekylibre.database.ZeroContract.GroupZones;
import ekylibre.database.ZeroContract.Interventions;
import ekylibre.database.ZeroContract.WorkingPeriodAttributes;
import ekylibre.util.AccountTool;
import ekylibre.util.ontology.Node;
import ekylibre.util.pojo.GenericEntity;
import ekylibre.util.pojo.ProcedureEntity;
import ekylibre.util.xml.ProcedureFamiliesXMLReader;
import ekylibre.util.xml.ProceduresXMLReader;
import ekylibre.zero.BuildConfig;
import ekylibre.zero.R;
import ekylibre.zero.home.Zero;
import ekylibre.zero.inter.fragment.CropParcelChoiceFragment;
import ekylibre.zero.inter.fragment.InterventionFormFragment;
import ekylibre.zero.inter.fragment.ParamChoiceFragment;
import ekylibre.zero.inter.fragment.ProcedureChoiceFragment;
import ekylibre.zero.inter.fragment.ProcedureFamilyChoiceFragment;
import ekylibre.zero.inter.fragment.SimpleChoiceFragment;
import ekylibre.zero.inter.model.CropParcel;
import ekylibre.zero.inter.model.GenericItem;
import ekylibre.zero.inter.model.Period;
import ekylibre.zero.inter.model.Work;
import ekylibre.zero.inter.model.Zone;
import ekylibre.zero.intervention.InterventionActivity;

import static ekylibre.util.Helper.getSingular;
import static ekylibre.util.Helper.getTranslation;
import static ekylibre.util.Helper.toIso8601Android;
import static ekylibre.util.Units.LITER;
import static ekylibre.util.Units.LITER_PER_HECTARE;
import static ekylibre.zero.home.Zero.ISO8601;
import static ekylibre.zero.home.Zero.LARRERE;
import static ekylibre.zero.inter.fragment.InterventionFormFragment.checkedActions;
import static ekylibre.zero.inter.fragment.InterventionFormFragment.description;
import static ekylibre.zero.inter.fragment.InterventionFormFragment.getSelectedArea;
import static ekylibre.zero.inter.fragment.InterventionFormFragment.selectedParamsList;
import static ekylibre.zero.inter.fragment.InterventionFormFragment.selectedVariantsList;
import static ekylibre.zero.inter.fragment.InterventionFormFragment.workList;
import static ekylibre.zero.inter.fragment.InterventionFormFragment.zoneList;


public class InterActivity extends AppCompatActivity implements FragmentManager.OnBackStackChangedListener,
        ProcedureFamilyChoiceFragment.OnFragmentInteractionListener,
        ProcedureChoiceFragment.OnFragmentInteractionListener,
        InterventionFormFragment.OnFragmentInteractionListener,
        SimpleChoiceFragment.OnFragmentInteractionListener {

    private static final String TAG = "NewInterventionActivity";

    public static final String PROCEDURE_FAMILY_FRAGMENT = "ekylibre.zero.fragments.procedure.family";
    public static final String PROCEDURE_CATEGORY_FRAGMENT = "ekylibre.zero.fragments.procedure.category";
    public static final String PROCEDURE_NATURE_FRAGMENT = "ekylibre.zero.fragments.procedure.nature";
    public static final String INTERVENTION_FORM = "ekylibre.zero.fragments.intervention.form";
    public static final String PARAM_FRAGMENT = "ekylibre.zero.fragments.intervention.param";

    public static final String PLANT = "plant";
    public static final String LAND_PARCEL = "land_parcel";
    public static final String CULTIVATION = "cultivation";
    public static final String ZONE = "zone";
    public static final String WORK = "work";
    public static final String FINISHED = "finished";
    private static final String WITH_TIMERS = "with_timers";

    private static final Pair<Integer,String> ADMINISTERING = Pair.create(R.id.administering, "administering");
    private static final Pair<Integer,String> ANIMAL_FARMING = Pair.create(R.id.animal_farming, "animal_farming");
    private static final Pair<Integer,String> PLANT_FARMING = Pair.create(R.id.plant_farming, "plant_farming");
    private static final Pair<Integer,String> PROCESSING = Pair.create(R.id.processing, "processing");
    private static final Pair<Integer,String> TOOL_MAINTAINING = Pair.create(R.id.tool_maintaining, "tool_maintaining");

    public static Account account;
    public static ActionBar actionBar;
    private String currentFragment;
    public static FragmentManager fragmentManager;

    public static Pair<Integer,String> currentFamily;
    public static Pair<String,String> currentCategory;
    public static List<ProcedureEntity> procedures;
    public static ProcedureEntity selectedProcedure;
    public static List<CropParcel> selectedCropParcels;
    public static List<GenericItem> selectedDrivers;
    public static boolean isContinuing;

    public static Node<String> ontology;
    public static Map<String,List<Pair<String,String>>> families;
    public static Map<String,List<Pair<String,String>>> natures;
    public static int workerId;

    @BindView(R.id.toolbar) Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inter);
        ButterKnife.bind(this);

        // Reset indicator continuing to working times record
        isContinuing = false;

        // Get current account
        account = AccountTool.getCurrentAccount(this);

        // Set toolbar
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.close);
        }

        // Init interventions parameters
        selectedCropParcels = new ArrayList<>();
        selectedDrivers = new ArrayList<>();

        // Get fragment manager
        fragmentManager = getSupportFragmentManager();
        fragmentManager.addOnBackStackChangedListener(this);

        // Set first fragment to procedure family choice
        replaceFragmentWith(PROCEDURE_FAMILY_FRAGMENT, null, null);

        // Load procedure natures, families and params from assets
        loadXMLAssets();

        // Get current user id
        SharedPreferences prefs = getSharedPreferences("prefs", Context.MODE_MULTI_PROCESS);
        String email = AccountTool.getEmail(account);
        workerId = prefs.getInt(email, 0);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * Method used to select the fragment to display
     * @param fragmentTag A tag representing a fragment type
     * @param filter Optional string containing current filter based on user selection
     * @param role What for the comming selection will be used
     */
    void replaceFragmentWith(String fragmentTag, String filter, String role) {

        // Update current fragment reference
        currentFragment = fragmentTag;
        invalidateOptionsMenu();

        FragmentTransaction ft = fragmentManager.beginTransaction();
        Fragment fragment;

        switch (fragmentTag) {

            case PROCEDURE_FAMILY_FRAGMENT:
                fragment = ProcedureFamilyChoiceFragment.newInstance();
                break;

            case PROCEDURE_CATEGORY_FRAGMENT:
            case PROCEDURE_NATURE_FRAGMENT:
                fragment = ProcedureChoiceFragment.newInstance(fragmentTag);
                break;

            case INTERVENTION_FORM:
                fragment = InterventionFormFragment.newInstance();
                break;

            case ZONE:
            case PLANT:
            case LAND_PARCEL:
            case CULTIVATION:
                fragment = CropParcelChoiceFragment.newInstance(fragmentTag, filter);
                break;

            case "zone_plant":
            case "zone_land_parcel":
            case "work_equipment":
                // Role = zoneId here ton keep ref to current zone edition
                // Group item position passed in role variable
                fragment = SimpleChoiceFragment.newInstance(fragmentTag, filter, role);
                break;

            default:
                // Default to param choice fragment list (inputs, equipments, workers...)
                fragment = ParamChoiceFragment.newInstance(fragmentTag, filter, role);
                break;
        }

//        ft.setCustomAnimations(R.anim.exit_to_left, R.anim.enter_from_right);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.replace(R.id.fragment_container, fragment, fragmentTag);
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void onProcedureFamilyInteraction(Integer id) {

        if (id.equals(ADMINISTERING.first))
            currentFamily = ADMINISTERING;
        else if (id.equals(ANIMAL_FARMING.first))
            currentFamily = ANIMAL_FARMING;
        else if (id.equals(PLANT_FARMING.first))
            currentFamily = PLANT_FARMING;
        else if (id.equals(PROCESSING.first))
            currentFamily = PROCESSING;
        else if (id.equals(TOOL_MAINTAINING.first))
            currentFamily = TOOL_MAINTAINING;

        replaceFragmentWith(PROCEDURE_CATEGORY_FRAGMENT, null, null);
    }

    public ProcedureEntity getProcedureItem(String name) {
        for (ProcedureEntity procedure : procedures)
            if (procedure.name.equals(name))
                return procedure;
        return null;
    }

    /**
     * Load interventions families from db.xml
     * and all procedures from procedure asset folder
     */
    private void loadXMLAssets() {
        try {
            procedures = loadProcedures();
            families = loadFamilies();

            // Removes childless categories (categories with no intervention)
            for (Map.Entry<String, List<Pair<String, String>>> family : families.entrySet()) {
                List<Pair<String, String>> categories = family.getValue();
                ListIterator<Pair<String, String>> it = categories.listIterator();
                while (it.hasNext()) {
                    if (!natures.containsKey(it.next().first))
                        it.remove();
                }
            }

        } catch (IOException | XmlPullParserException e) {
            e.printStackTrace();
        }
    }

    private List<ProcedureEntity> loadProcedures() throws IOException, XmlPullParserException {

        final List<ProcedureEntity> procedureList = new ArrayList<>();
        String[] files = getAssets().list("procedures");
        natures = new ArrayMap<>();

        if (files != null) {
            for (String asset : files) {
                ProceduresXMLReader proceduresXmlReader = new ProceduresXMLReader();
                InputStream is = getAssets().open("procedures/" + asset);
                ProcedureEntity procedureEntity = proceduresXmlReader.parse(is);

                if (!procedureEntity.deprecated && !procedureEntity.hidden) {

                    procedureList.add(procedureEntity);

                    // Fill procedure natures map
                    String name = procedureEntity.name;
                    int resId = getResources().getIdentifier(name, "string", Zero.getPkgName());
                    Pair<String, String> procedureNaturePair = Pair.create(name, getString(resId));

                    for (String category : procedureEntity.categories) {
                        if (natures.containsKey(category)) {
                            List<Pair<String, String>> proceduresList = natures.get(category);
                            if (proceduresList == null)
                                proceduresList = new ArrayList<>();
                            proceduresList.add(procedureNaturePair);
                            natures.put(category, proceduresList);
                        } else {
                            natures.put(category, new ArrayList<>(Collections.singletonList(procedureNaturePair)));
                        }
                    }
                }
            }
        }
        return procedureList;
    }

    private Map<String,List<Pair<String,String>>> loadFamilies() throws IOException, XmlPullParserException {
        InputStream is = getAssets().open("db.xml");
        ProcedureFamiliesXMLReader xmlReader = new ProcedureFamiliesXMLReader(getBaseContext());
        return xmlReader.parse(is);
    }

    @Override
    public void onFormFragmentInteraction(String fragmentTag, String filter, String role) {
        replaceFragmentWith(fragmentTag, filter, role);
    }

    @Override
    public void onItemChoosed(Pair<String,String> item, String fragmentRef) {

        if (PROCEDURE_CATEGORY_FRAGMENT.equals(fragmentRef)) {
            if (BuildConfig.DEBUG)
                Log.i(TAG, "Category -> " + item);
            currentCategory = item;

            List<Pair<String, String>> _procedures = natures.get(item.first);
            if (_procedures != null && _procedures.size() == 1) {
                Pair<String, String> _item = _procedures.get(0);
                selectedProcedure = getProcedureItem(_item.first);
                replaceFragmentWith(INTERVENTION_FORM, null, null);
            } else
                replaceFragmentWith(PROCEDURE_NATURE_FRAGMENT, null, null);
        } else {
            if (BuildConfig.DEBUG)
                Log.i(TAG, "Procedure -> " + item);
            selectedProcedure = getProcedureItem(item.first);
            replaceFragmentWith(INTERVENTION_FORM, null, null);
        }
    }

    @Override
    public void onItemChoosed() {
        previousFragmentOrQuit();
    }

    /** *****************************************************************
     *                                                                  *
     *                      NAVIGATION MANAGEMENT                       *
     *                                                                  *
     ***************************************************************** **/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.clear();
        if (BuildConfig.DEBUG) Log.v(TAG, "onCreateOptionMenu");
//        if (currentFragment.equals(INTERVENTION_FORM)) {
//            menu.clear();
//            menu.add()
////            MenuInflater inflater = getMenuInflater();
////            inflater.inflate(R.menu.intervention_options_menu, menu);
//        }
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.intervention_options_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        Log.v(TAG, "onPrepareOptionMenu");
        switch (currentFragment) {
            case INTERVENTION_FORM:
                menu.findItem(R.id.action_inter_save).setVisible(true);
                break;
            case PARAM_FRAGMENT:
            case PLANT:
            case LAND_PARCEL:
            case CULTIVATION:
            case ZONE:
                menu.findItem(R.id.action_inter_done).setVisible(true);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    private void previousFragmentOrQuit() {
        // Finish the activity if on first fragment else popBackStack()
        Fragment f = fragmentManager.findFragmentById(R.id.fragment_container);
        if (f instanceof ProcedureFamilyChoiceFragment)
            finish();
        fragmentManager.popBackStack();
    }

    @Override
    public void onBackStackChanged() {
        Fragment f = fragmentManager.findFragmentById(R.id.fragment_container);

        if (f instanceof InterventionFormFragment)
            currentFragment = INTERVENTION_FORM;
        else if (f instanceof ParamChoiceFragment)
            currentFragment = PARAM_FRAGMENT;
        else if (f instanceof ProcedureChoiceFragment)
            currentFragment = PROCEDURE_NATURE_FRAGMENT;

        if (BuildConfig.DEBUG)
            Log.v(TAG, "onBackStackChanged - current fragment is -> " + currentFragment);



        invalidateOptionsMenu();

        // Change icon to close if on first fragment
        int backStack = fragmentManager.getBackStackEntryCount();
        if (backStack > 1 )
            actionBar.setHomeAsUpIndicator(R.drawable.ic_back);
        else
            actionBar.setHomeAsUpIndicator(R.drawable.close);

//        Fragment f = fragmentManager.findFragmentById(R.id.fragment_container);

        // Force reset the option menu
//        invalidateOptionsMenu();
    }

    /**
     *      Back and Up navigation will do the same thing
     */
    @Override
    public boolean onSupportNavigateUp() {
        previousFragmentOrQuit();
        return true;
    }

    @Override
    public void onBackPressed() {
        previousFragmentOrQuit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_inter_save:
                if (currentFragment.equals(INTERVENTION_FORM))
                    validateIntervention();
                break;

            case R.id.action_inter_done:
                previousFragmentOrQuit();
                break;

//            case R.id.action_phyto_info:
//                FragmentTransaction ft = fragmentManager.beginTransaction();
//                PhytosanitaryDialogFragment phytoDialog = PhytosanitaryDialogFragment.newInstance(2060118);
//                phytoDialog.show(ft, null);
//                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

//    /**
//     * Round a Period object to minute and return a Calendar instance
//     * @return Calendar
//     */
//    private Calendar comparableDateTime(Period period) {
//        Calendar cal = Calendar.getInstance();
//        cal.setTime(period.startDateTime);
//        cal.set(Calendar.SECOND, 0);
//        cal.set(Calendar.MILLISECOND, 0);
//        cal.set(Calendar.ZONE_OFFSET, 0);
//
//    }

    /**
     * Method used to validate intervention form
     * The valid boolean flag must stay true to continue saving intervention
     * New validation process can be added here
     */
    private void validateIntervention() {

        Log.i(TAG, "Start validation process before saving in database.");

        if (BuildConfig.DEBUG) {
            for (GenericItem item : selectedParamsList)
                Log.i(TAG, "paramList item=" + item.name + " | ref=" + item.referenceName);
            for (GenericItem item : selectedVariantsList)
                Log.i(TAG, "variantList item=" + item.name + " | ref=" + item.referenceName);
            for (Work work : workList)
                if (work.equipment != null)
                    Log.d(TAG, "Work equipment -> " + work.equipment.name + " | " + work.equipment.referenceName);
        }

        // ----------- //
        // VALIDATIONS //
        // ----------- //

        boolean valid = true;
        int counter;
        BigDecimal zero = new BigDecimal("0");

        periodLoop:
        for (Period period1 : InterventionFormFragment.periodList) {

            Date start = DateUtils.round(period1.startDateTime, Calendar.MINUTE);
            Date stop = DateUtils.round(period1.stopDateTime, Calendar.MINUTE);

            Log.i(TAG, "Dates -> "+start.getTime()+" | "+stop.getTime());

            if (start.compareTo(stop) == 0) {
                valid = false;
                displayWarningDialog(getString(R.string.dates_must_be_different));
                break;
            }

            // Check for period overlapping
            for (Period period2 : InterventionFormFragment.periodList)
                if (period2 != period1 && overlaps(period1, period2)) {
                    valid = false;
                    displayWarningDialog(getString(R.string.warning_periods_overlaps));
                    break periodLoop;
                }
        }

        // Check if one culture/parcel is set
        if (valid && selectedProcedure.target.size() > 0) {

            for (GenericEntity target : selectedProcedure.target) {
                Log.i(TAG, "Validate " + target.name);

                counter = 0;
                for (GenericItem item : selectedParamsList) {

                    Log.i(TAG, "ReferenceName -> " + item.referenceName);

                    // If target correspond to procedure
                    if (item.referenceName.containsKey(target.name)
                            || (item.referenceName.containsKey("zone") && item.referenceName.containsValue("targets"))
                            || (item.referenceName.containsKey("work") && item.referenceName.containsValue("targets"))) {
                        Log.i(TAG, "Found " + target.name);
                        ++counter;

                        // Check periods contained in productions dates
                        if (item.born_at != null) {
                            Log.i(TAG, "born_at = " + item.born_at);
                            for (Period period : InterventionFormFragment.periodList) {
                                if (!isContained(period, item.born_at, item.dead_at)) {
                                    valid = false;
                                    displayWarningDialog(getString(R.string.period_does_not_match_production));
                                    break;
                                }
                            }
                        } else
                            Log.w(TAG, "No dates to check");
                        break;
                    }
                }

                // Check for work group targets
                for (Work work : workList)
                    if (work.equipment != null)
                        ++counter;

                // Check for zone group targets
                for (Zone zone : zoneList)
                    if (zone.landParcel != null)
                        ++counter;

                // Check there is one target at least
                // Check if this target is mandatory (in cardinality)
                if (target.cardinality == null || target.cardinality.equals("1")) {
                    if (counter == 0) {
                        valid = false;
                        if (selectedProcedure.group != null && selectedProcedure.group.equals(ZONE) && zoneList.isEmpty())
                            displayWarningDialog("Vous devez ajouter les paramètres de zone");
                        else if (selectedProcedure.group != null && selectedProcedure.group.equals(WORK) && workList.isEmpty())
                            displayWarningDialog("Vous devez ajouter les paramètres de maintenance");
                        else
                            displayWarningDialog(getString(R.string.at_least_one) + " " + getSingular(target.name));
                        break;
                    } else
                        Log.i(TAG, "Validation OK for " + target.name);
                }
            }
        }

        // Check if input needed and a quantity is set
        if (valid && selectedProcedure.input.size() > 0) {

            for (GenericEntity input : selectedProcedure.input) {
                Log.d(TAG, "Validate " + input.name);

                counter = 0;
                for (GenericItem item : selectedParamsList) {

                    if (item.referenceName.containsKey(input.name)) {
                        ++counter;
                        if (item.quantity == null || item.quantity.compareTo(zero) <= 0) {
                            valid = false;
                            displayWarningDialog(getString(R.string.set_quantity_for_input));
                            break;
                        }
                    }
                }
                // Check the is one input at least
                if (input.cardinality == null || input.cardinality.equals("1")) {
                    if (counter == 0) {
                        valid = false;
                        displayWarningDialog(getString(R.string.set_at_least_one)+" "+ getSingular(input.name));
                        break;
                    } else
                        Log.i(TAG, "Validation OK for " + input.name);
                }
            }
        }

        // Check if output needed and a quantity is set
        if (valid && selectedProcedure.output.size() > 0) {

            for (GenericEntity output : selectedProcedure.output) {
                Log.d(TAG, "Validate " + output.name);

                counter = 0;
                for (GenericItem item : selectedVariantsList) {

                    if (item.referenceName.containsKey(output.name)
                            || (item.referenceName.containsKey(ZONE) && item.referenceName.containsValue("outputs"))) {
                        ++counter;
                        if (!item.referenceName.containsKey(ZONE)) {
                            if (item.quantity == null || item.quantity.compareTo(zero) <= 0) {
                                valid = false;
                                displayWarningDialog(getString(R.string.set_output_quantity) + getSingular(output.name) + "\"");
                                break;
                            }
                        }
                    }
                }

                // Check for output in zone group
                for (Zone zone : zoneList)
                    if (zone.plant != null)
                        ++counter;
                // Check the is one input at least
                if (output.cardinality == null || output.cardinality.equals("1")) {
                    if (counter == 0) {
                        valid = false;
                        displayWarningDialog(getString(R.string.set_at_least_one_output) + getSingular(output.name) + "\"");
                        break;
                    } else
                        Log.i(TAG, "Validation OK for " + output.name);
                }
            }
        }

        // Check if doer needed and a quantity is set
        if (valid && selectedProcedure.doer.size() > 0) {

            for (GenericEntity doer : selectedProcedure.doer) {
                Log.d(TAG, "Validate " + doer.name);

                counter = 0;
                for (GenericItem item : selectedParamsList)
                    if (item.referenceName.containsKey(doer.name))
                        ++counter;

                // Check the is one input at least
                if (doer.cardinality == null || doer.cardinality.equals("1"))
                    if (counter == 0) {
                        valid = false;
                        displayWarningDialog(String.format(
                                "%s %s", getString(R.string.set_one), getSingular(doer.name)));
                        break;
                    } else
                        Log.i(TAG, "Validation OK for " + doer.name);
            }
        }

        // Check if tool needed and a quantity is set
        if (valid && selectedProcedure.tool.size() > 0) {

            for (GenericEntity tool : selectedProcedure.tool) {
                Log.d(TAG, "Validate " + tool.name);

                counter = 0;
                for (GenericItem item : selectedParamsList)
                    if (item.referenceName.containsKey(tool.name))
                        ++counter;

                // Check the is one input at least
                if (tool.cardinality == null || tool.cardinality.equals("1"))
                    if (counter == 0) {
                        valid = false;
                        displayWarningDialog(getString(R.string.set_at_least_one_tool)+" "+ getSingular(tool.name)+ "\"");
                        break;
                    } else
                        Log.i(TAG, "Validation OK for " + tool.name);
            }
        }

        if (valid) {
            // Display modale to continue to working times recording if wanted
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getString(R.string.save_now_or_start_recording_times));
            builder.setNegativeButton(R.string.save, (dialog, i) -> {
                // Mandatory hour conters only for larrere flavor
            if (BuildConfig.FLAVOR.equals(LARRERE)) {
                    boolean ok = true;
                    if (!selectedProcedure.name.equals("fuel_up")) {
                        for (GenericItem item : selectedParamsList) {
                            if (item.hasHourMeter && (item.hourMeter == null || item.hourMeter.compareTo(new BigDecimal(0)) <= 0))
                                ok = false;
                        }
                    }

                    // Check for equipment in work group
                    if (selectedProcedure.group != null && selectedProcedure.group.equals("work")) {
                        for (Work work : workList) {
                            if (work.equipment.hasHourMeter
                                    && (work.equipment.hourMeter == null
                                    || work.equipment.hourMeter.compareTo(new BigDecimal(0)) <= 0))
                                ok = false;
                        }
                    }
                    if (ok)
                        saveIntervention(false);
                    else {
                        Snackbar snack =
                                Snackbar.make(findViewById(R.id.fragment_container),
                                        getResources().getString(R.string.hour_counter_warning),
                                        Snackbar.LENGTH_INDEFINITE);
                        snack.setAction(getResources().getString(R.string.ok), view -> snack.dismiss());
                        snack.show();
                    }
                } else
                    saveIntervention(false);
            });
            builder.setPositiveButton(getResources().getString(R.string.start), (dialog, i) -> saveIntervention(true));
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    /**
     * Save the current intervention
     * @param continueWithTimers A flag to continue of not with working periods timers
     */
    private void saveIntervention(boolean continueWithTimers) {

        // Get content resolver instance
        ContentResolver cr = getContentResolver();

        // -------------------------- //
        // Creating base intervention //
        // -------------------------- //
        ContentValues cv = new ContentValues();
        cv.put(DetailedInterventions.PROCEDURE_NAME, selectedProcedure.name);
        cv.put(DetailedInterventions.CREATED_ON, new Date().getTime());
        cv.put(DetailedInterventions.USER, AccountTool.getInstance(account));
        cv.put(DetailedInterventions.STATUS, continueWithTimers ? WITH_TIMERS : FINISHED);

        if (description != null)
            cv.put(DetailedInterventions.DESCRIPTION, description);

        if (!checkedActions.isEmpty())
            cv.put(DetailedInterventions.ACTIONS, TextUtils.join(",", checkedActions));

        // Insert into database & get returning id
        Uri uri = cr.insert(DetailedInterventions.CONTENT_URI, cv);
        long interId = ContentUris.parseId(Objects.requireNonNull(uri));
//            long interId = -1;
//            if (uri != null && uri.getLastPathSegment() != null)
//                interId = Long.valueOf(uri.getLastPathSegment());
        if (BuildConfig.DEBUG)
            Log.i(TAG, "Intervention id = " + interId);


        // ------------ //
        // Working time //
        // ------------ //

//        final SimpleDateFormat dateFormatter = new SimpleDateFormat(DateConstant.ISO_8601, getCurrentLocale(this));

        List<ContentValues> bulkPeriodCv = new ArrayList<>();

        for (Period period : InterventionFormFragment.periodList) {

            ContentValues periodCv = new ContentValues();
            periodCv.put(WorkingPeriodAttributes.DETAILED_INTERVENTION_ID, interId);
//            periodCv.put(WorkingPeriodAttributes.STARTED_AT, period.startDateTime.getTime());
//            periodCv.put(WorkingPeriodAttributes.STOPPED_AT, period.stopDateTime.getTime());
            periodCv.put(WorkingPeriodAttributes.STARTED_AT, toIso8601Android(ISO8601.format(period.startDateTime.getTime())));
            periodCv.put(WorkingPeriodAttributes.STOPPED_AT, toIso8601Android(ISO8601.format(period.stopDateTime.getTime())));
            bulkPeriodCv.add(periodCv);
        }
        cr.bulkInsert(WorkingPeriodAttributes.CONTENT_URI, bulkPeriodCv.toArray(new ContentValues[0]));


        // ---------------- //
        // Group parameters //
        // ---------------- //

        if (selectedProcedure.group != null) {
            Log.i(TAG, "group -> " + selectedProcedure.group);
            if (selectedProcedure.group.equals("zone")) {
                for (Zone zone : InterventionFormFragment.zoneList) {

                    // Save new zone group
                    ContentValues zoneCv = new ContentValues();
                    zoneCv.put(GroupZones.DETAILED_INTERVENTION_ID, interId);
                    zoneCv.put(GroupZones.NEW_NAME, zone.newName);
                    zoneCv.put(GroupZones.BATCH_NUMBER, zone.batchNumber);

                    // Insert into database & get returning id
                    Uri zoneUri = cr.insert(GroupZones.CONTENT_URI, zoneCv);
                    long zoneId = ContentUris.parseId(Objects.requireNonNull(zoneUri));

                    // Save target and get id back
                    Uri targetUri = cr.insert(DetailedInterventionAttributes.CONTENT_URI,
                            getContentValue(interId, zone.landParcel, LAND_PARCEL, "targets", zoneId));
                    long targetId = ContentUris.parseId(Objects.requireNonNull(targetUri));

                    // Save output and get id back
                    Uri outputUri = cr.insert(DetailedInterventionAttributes.CONTENT_URI,
                            getContentValue(interId, zone.plant, PLANT, "outputs", zoneId));
                    long outputId = ContentUris.parseId(Objects.requireNonNull(outputUri));

                    // Updates current zone with created ids
                    zoneCv.put(GroupZones.TARGET_ID, targetId);
                    zoneCv.put(GroupZones.OUTPUT_ID, outputId);

                    cr.update(zoneUri, zoneCv, null, null);
                }
            } else if (selectedProcedure.group.equals("work")) {
                for (Work work : InterventionFormFragment.workList) {

                    // Save new work group
                    ContentValues workCv = new ContentValues();
                    workCv.put(GroupWorks.DETAILED_INTERVENTION_ID, interId);

                    // Insert into database & get returning id
                    Uri workUri = cr.insert(GroupWorks.CONTENT_URI, workCv);
                    long workId = ContentUris.parseId(Objects.requireNonNull(workUri));

                    // Save target and get id back
                    Uri targetUri = cr.insert(DetailedInterventionAttributes.CONTENT_URI,
                            getContentValue(interId, work.equipment, "equipment", "targets", workId));
                    long targetId = ContentUris.parseId(Objects.requireNonNull(targetUri));

                    // Save input and get id back
                    List<Long> inputsIds = new ArrayList<>();

                    for (GenericItem item : work.replacementParts) {
                        Uri inputUri = cr.insert(DetailedInterventionAttributes.CONTENT_URI,
                                getContentValue(interId, item, "replacement_part", "inputs", workId));
                        inputsIds.add(ContentUris.parseId(Objects.requireNonNull(inputUri)));
                    }

                    for (GenericItem item : work.consumableParts) {
                        Uri inputUri = cr.insert(DetailedInterventionAttributes.CONTENT_URI,
                                getContentValue(interId, item, "consumable_part", "inputs", workId));
                        inputsIds.add(ContentUris.parseId(Objects.requireNonNull(inputUri)));
                    }

                    // Updates current work group with created ids
                    workCv.put(GroupWorks.TARGET_ID, targetId);
                    workCv.put(GroupWorks.INPUTS_ID, TextUtils.join(",", inputsIds));

                    cr.update(workUri, workCv, null, null);
                }
            }
        }

        // ----------------------- //
        // Intervention parameters //
        // ----------------------- //

        List<ContentValues> bulkParamCv = new ArrayList<>();

        for (GenericItem param : selectedParamsList)
            for (Map.Entry<String, String> entry : param.referenceName.entrySet()) {

                if (BuildConfig.DEBUG)
                    Log.d(TAG, "[product] " + param.name + " -> " + entry.getKey() + "::" + entry.getValue());

                // Skip param if included in work group
                if (selectedProcedure.name.equals("equipment_maintenance") && entry.getValue().equals("0"))
                    continue;

                bulkParamCv.add(getContentValue(interId, param, entry.getKey(), entry.getValue(), null));
            }

        for (GenericItem param : selectedVariantsList)
            for (Map.Entry<String, String> entry : param.referenceName.entrySet()) {

                if (BuildConfig.DEBUG)
                    Log.e(TAG, "[variant] " + param.name + " -> " + entry.getKey() + "::" + entry.getValue());

                if (!entry.getKey().equals("zone") && !entry.getKey().equals("work"))
                    bulkParamCv.add(getContentValue(interId, param, entry.getKey(), entry.getValue(), null));
            }

        // Insert into database & get returning id
        cr.bulkInsert(DetailedInterventionAttributes.CONTENT_URI, bulkParamCv.toArray(new ContentValues[0]));

        // Display modal to choose wether to quit or continue to record times
        if (continueWithTimers) {

            Period period = InterventionFormFragment.periodList.get(0);
            // Save reference to detailed intervention in intervention table
            ContentValues refCv = new ContentValues();
            refCv.put(Interventions.DETAILED_INTERVENTION_ID, interId);
            refCv.put(Interventions.PROCEDURE_NAME, selectedProcedure.name);
            refCv.put(Interventions.NAME, getTranslation(selectedProcedure.name));
            refCv.put(Interventions.DESCRIPTION, description);
            refCv.put(Interventions.STARTED_AT, toIso8601Android(period.startDateTime));
            refCv.put(Interventions.STOPPED_AT, toIso8601Android(period.stopDateTime));
            refCv.put(Interventions.USER, account.name);

            // Insert into database & get returning id
            Uri refUri = cr.insert(Interventions.CONTENT_URI, refCv);
            long refId = ContentUris.parseId(Objects.requireNonNull(refUri));

            // Create intent and start Intervention activity
            Intent intent = new Intent(this, InterventionActivity.class);
            intent.putExtra(Interventions._ID, (int) refId);
//            intent.putExtra("detailed_intervention_id", interId);
//            intent.putExtra("detailed_procedure_name", selectedProcedure.name);
            finish();
            startActivity(intent);
        } else {
            Toast.makeText(this, getString(R.string.intervention_well_saved), Toast.LENGTH_LONG).show();
            finish();
        }
    }

    private ContentValues getContentValue(long interId, GenericItem param, String refName, String role, Long groupId) {
        ContentValues paramCv = new ContentValues();
        paramCv.put(DetailedInterventionAttributes.DETAILED_INTERVENTION_ID, interId);
        paramCv.put(DetailedInterventionAttributes.REFERENCE_ID, param.id);
        paramCv.put(DetailedInterventionAttributes.REFERENCE_NAME, refName);
        paramCv.put(DetailedInterventionAttributes.NAME, param.name);
        paramCv.put(DetailedInterventionAttributes.ROLE, role);

        if (groupId != null)
            paramCv.put(DetailedInterventionAttributes.GROUP_ID, groupId);

        if (param.quantity != null) {
            if (param.phytoUnit != null) {
                if (param.phytoUnit.dimension.equals(LITER.dimension)) {
                    param.phytoUnit = LITER_PER_HECTARE;
                    param.quantity = param.quantity.divide(getSelectedArea(), 4, RoundingMode.CEILING);
                } else {
                    param.unit = param.phytoUnit.dimension;
                    param.unitString = param.phytoUnit.name;
                }
                paramCv.put(DetailedInterventionAttributes.UNIT_NAME, param.unitString);
                paramCv.put(DetailedInterventionAttributes.QUANTITY_INDICATOR, param.phytoUnit.dimension);
            } else {
                if (param.unit_spinner != null) {
                    if (param.unit_spinner.unit == null)
                        paramCv.put(DetailedInterventionAttributes.QUANTITY_NAME, "population");
                    else if (param.unit_spinner.name != null)
                        paramCv.put(DetailedInterventionAttributes.QUANTITY_NAME, param.unit_spinner.name);
                    else
                        paramCv.put(DetailedInterventionAttributes.QUANTITY_INDICATOR, param.unit_spinner.indicator);
                }
                // Get converted quantity to base unit
                //BigDecimal qtt = getQuantityInBaseUnit(param);
            }
            paramCv.put(DetailedInterventionAttributes.QUANTITY_VALUE, param.quantity.toString());
        }

        if (param.hourMeter != null)
            paramCv.put(DetailedInterventionAttributes.HOUR_METER, param.hourMeter.toString());

        if (param.phytoUsage != null && !param.phytoUsage.id.equals("-1"))
            paramCv.put(DetailedInterventionAttributes.PHYTO_USAGE, param.phytoUsage.id);

        return paramCv;
    }

    private boolean overlaps(Period period1, Period period2){
        return period1.startDateTime.getTime() <= period2.stopDateTime.getTime()
                && period2.startDateTime.getTime() <= period1.stopDateTime.getTime();
    }

    private boolean isContained(Period period, Date start, Date stop) {
        if (stop == null)
            stop = new Date(4102444800000L);
        return period.startDateTime.getTime() >= start.getTime() && period.startDateTime.getTime() <=stop.getTime()
                && period.stopDateTime.getTime() >= start.getTime() && period.stopDateTime.getTime() <= stop.getTime();
    }

    private void displayWarningDialog(String text) {
//        AlertDialog.Builder builder = new AlertDialog.Builder(context);
//        builder.setMessage(text);
//        builder.setPositiveButton("Ok", (dialog, i) -> dialog.cancel());
//        AlertDialog dialog = builder.create();
//        dialog.show();

        Snackbar snack = Snackbar.make(
                findViewById(R.id.fragment_container), text, Snackbar.LENGTH_LONG);
        snack.setAction("OK", view -> snack.dismiss());
        snack.show();
    }
}
