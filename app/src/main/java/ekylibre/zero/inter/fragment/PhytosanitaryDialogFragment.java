package ekylibre.zero.inter.fragment;


import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ekylibre.database.ZeroContract.PhytosanitaryReferences;
import ekylibre.database.ZeroContract.PhytosanitaryUsages;
import ekylibre.zero.R;
import ekylibre.zero.inter.adapter.PhytosanitaryUsageAdapter;
import ekylibre.zero.inter.model.PhytosanitaryDetails;
import ekylibre.zero.inter.model.PhytosanitaryUsage;

import static ekylibre.zero.inter.model.PhytosanitaryDetails.getPhytosanitaryDetails;
import static ekylibre.zero.inter.model.PhytosanitaryUsage.getPhytosanitaryUsage;

public class PhytosanitaryDialogFragment extends DialogFragment {

    private ContentResolver cr;
    private Context context;
    private int productId;

    @BindView(R.id.name) TextView productName;
    @BindView(R.id.firm) TextView productFirm;
    @BindView(R.id.other_name_chips_group) ChipGroup otherNameChipGroup;
    @BindView(R.id.function) TextView productFunction;
    @BindView(R.id.amm) TextView productAmm;
    @BindView(R.id.dre) TextView productDre;
    @BindView(R.id.compounds) TextView productCompounds;
    @BindView(R.id.protection_mention) TextView productMention;
    @BindView(R.id.usages_recycler) RecyclerView usagesRecycler;

    @OnClick(R.id.close)
    public void close() {
        Dialog dialog = getDialog();
        if (dialog != null)
            dialog.dismiss();
    }

    public static PhytosanitaryDialogFragment newInstance(int id) {

        PhytosanitaryDialogFragment dialog = new PhytosanitaryDialogFragment();
        Bundle args = new Bundle();
        args.putInt("product_id", id);
        dialog.setArguments(args);

        return dialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getContext();
        if (getArguments() != null)
            productId = getArguments().getInt("product_id");
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        if (window != null)
            window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_fragment_phyto_info, container, false);

        // Bind to the inflated view
        ButterKnife.bind(this, view);

        cr = context.getContentResolver();
        usagesRecycler.setLayoutManager(new LinearLayoutManager(context));
        usagesRecycler.setNestedScrollingEnabled(true);

        PhytosanitaryDetails phytoRef = getPhytosanitaryReferenceFromDB(productId);

        if (phytoRef != null)
            setBaseInfo(phytoRef);

        return view;
    }

    private void setBaseInfo(PhytosanitaryDetails product) {

        if (product.otherName != null) {
            for (String name : product.otherName.split(" \\| "))
                otherNameChipGroup.addView(buildChip(name));
        } else {
            otherNameChipGroup.setVisibility(View.GONE);
        }

        productName.setText(product.name);
        productFirm.setText(product.firmName);
        productFunction.setText(product.nature != null ? product.nature : "--");
        productAmm.setText(String.valueOf(product.id));

        if (product.fieldReentryDelay != null)
            productDre.setText(getString(R.string.x_hours, product.fieldReentryDelay));
        else
            productDre.setVisibility(View.GONE);

        if (product.activeCompounds != null)
            productCompounds.setText(product.activeCompounds.replaceAll(" \\| ", " \n"));
        else
            productCompounds.setVisibility(View.GONE);

        if (product.operatorProtection != null) {
            productMention.setText(product.operatorProtection);
            productMention.setMovementMethod(new ScrollingMovementMethod());
        } else
            productMention.setVisibility(View.GONE);

        if (product.usages != null && !product.usages.isEmpty()) {
            PhytosanitaryUsageAdapter usagesAdapter = new PhytosanitaryUsageAdapter(product.usages);
            usagesRecycler.setAdapter(usagesAdapter);
        }

    }

    private Chip buildChip(String name) {
        Chip chip = (Chip) getLayoutInflater().inflate(R.layout.phyto_chip_layout, otherNameChipGroup, false);
        chip.setText(name);
        return  chip;
    }

    private PhytosanitaryDetails getPhytosanitaryReferenceFromDB(int id) {

        List<PhytosanitaryUsage> usages = new ArrayList<>();
        PhytosanitaryDetails phytoDetails = null;

        try (Cursor curs = cr.query(PhytosanitaryUsages.CONTENT_URI, PhytosanitaryUsages.PROJECTION_ALL,
                String.format("product_id = %s", id), null, PhytosanitaryUsages.ORDER_BY_STATE)) {

            if (curs != null && curs.getCount() > 0) {
                while (curs.moveToNext())
                    usages.add(getPhytosanitaryUsage(curs));
            }
        }

        Log.i("Phyto info dialog", "There is #" + usages.size() + " usages.");

        try (Cursor curs = cr.query(PhytosanitaryReferences.CONTENT_URI, PhytosanitaryReferences.PROJECTION_ALL,
                String.format("id = %s", id), null, null)) {

            if (curs != null && curs.moveToFirst()) {
                phytoDetails = getPhytosanitaryDetails(curs);
                phytoDetails.usages = usages;
            }
        }


        return phytoDetails;
    }

}