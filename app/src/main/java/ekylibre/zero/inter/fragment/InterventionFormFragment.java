package ekylibre.zero.inter.fragment;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ekylibre.database.ZeroContract;
import ekylibre.database.ZeroContract.PhytosanitaryUsages;
import ekylibre.database.ZeroContract.PhytosanitaryReferences;
import ekylibre.database.ZeroContract.EphyCropsets;
import ekylibre.util.AccountTool;
import ekylibre.util.Helper;
import ekylibre.util.layout.MarginTopItemDecoration;
import ekylibre.util.layout.component.WidgetParamView;
import ekylibre.util.ontology.Ontology;
import ekylibre.util.pojo.GenericEntity;
import ekylibre.zero.BuildConfig;
import ekylibre.zero.R;
import ekylibre.zero.home.Zero;
import ekylibre.zero.inter.InterActivity;
import ekylibre.zero.inter.adapter.FormPeriodAdapter;
import ekylibre.zero.inter.adapter.PhytosanitaryProductAdapter;
import ekylibre.zero.inter.adapter.QuantityItemAdapter;
import ekylibre.zero.inter.adapter.WorkAdapter;
import ekylibre.zero.inter.adapter.ZoneAdapter;
import ekylibre.zero.inter.model.GenericItem;
import ekylibre.zero.inter.model.Period;
import ekylibre.zero.inter.model.PhytosanitaryDetails;
import ekylibre.zero.inter.model.PhytosanitaryUsage;
import ekylibre.zero.inter.model.Work;
import ekylibre.zero.inter.model.Zone;

import static ekylibre.zero.inter.InterActivity.account;
import static ekylibre.zero.inter.InterActivity.selectedProcedure;
import static ekylibre.zero.inter.InterActivity.workerId;
import static ekylibre.zero.inter.model.PhytosanitaryDetails.getPhytosanitaryDetails;
import static ekylibre.zero.inter.model.PhytosanitaryUsage.getPhytosanitaryUsage;


public class InterventionFormFragment extends Fragment {

    private static final String TAG = "InterventionFormFragmen";
    private static final String OUTPUTS = "outputs";
    private static final String INPUTS = "inputs";
    private static final String EQUIPMENTS = "equipments";
    private static final String ZONE = "zone";
    private static final String WORK = "work";
    private static final String TARGETS = "targets";
    private static final String WORKERS = "workers";


    private Context context;
    private OnFragmentInteractionListener listener;
    private static PhytosanitaryProductAdapter phytoAdapter;
    private static int scrollPosition = 0;

    public static List<GenericItem> selectedParamsList;
    public static List<GenericItem> selectedVariantsList;
    public static List<PhytosanitaryDetails> selectedPhytosanitaryList;

    public static List<Period> periodList;
    public static List<Zone> zoneList;
    public static List<Work> workList;
    public static List<String> checkedActions;
    public static String description;

    // LAYOUT BINDINGS
    @BindView(R.id.widgets_container) LinearLayoutCompat widgetContainer;
    @BindView(R.id.form_period_recycler) RecyclerView periodRecycler;
    @BindView(R.id.form_period_add) TextView addPeriod;
    @BindView(R.id.scroll_view) NestedScrollView scrollView;
//    @BindView(R.id.loading_content) ProgressBar loadingProgressBar;


    public static InterventionFormFragment newInstance() {
        return new InterventionFormFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        // Get context when fragment is attached
        this.context = context;

        if (context instanceof OnFragmentInteractionListener)
            listener = (OnFragmentInteractionListener) context;
        else
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (BuildConfig.DEBUG)
            Log.v(TAG, "onCreate");

//        ContentResolver cr = context.getContentResolver();

        // Add one hour period as default
        periodList = new ArrayList<>();
        periodList.add(new Period());

        zoneList = new ArrayList<>();
        zoneList.add(new Zone());

        checkedActions = new ArrayList<>();

        workList = new ArrayList<>();
        workList.add(new Work());

        selectedParamsList = new ArrayList<>();
        selectedVariantsList = new ArrayList<>();

        description = null;

        setDriverIfExists();

//        paramsList.addAll(getProducts(cr));
//        variantsList.addAll(getVariants(cr));
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (BuildConfig.DEBUG) {
            Log.v(TAG, "onCreateView");
            Log.v(TAG, "selectedParamsList -> " + selectedParamsList.size());
            Log.i(TAG, "Selected crops -> " + getSelectedCrops());
            for (GenericItem it : selectedParamsList)
                Log.i(TAG, String.format("[PRODUCT]-> %s %s %s", it.id, it.name, it.referenceName));
            for (GenericItem it : selectedVariantsList)
                Log.i(TAG, String.format("[VARIANT]-> %s %s %s", it.id, it.name, it.referenceName));
        }

        // Set title
        InterActivity.actionBar.setTitle(Helper.getStringId(selectedProcedure.name));

        // Inflate layout
        View view = inflater.inflate(R.layout.fragment_intervention_form, container, false);
        ButterKnife.bind(this, view);

        // ------------------ //
        // Time Period layout //
        // ------------------ //

        // Set RecyclerView and associated Adapter
        periodRecycler.setLayoutManager(new LinearLayoutManager(context));
        periodRecycler.addItemDecoration(new MarginTopItemDecoration(context, 16));
        periodRecycler.setNestedScrollingEnabled(false);
        FormPeriodAdapter periodAdapter = new FormPeriodAdapter(periodList);
        periodRecycler.setAdapter(periodAdapter);

        addPeriod.setOnClickListener(v -> {
            periodList.add(new Period());
            periodAdapter.notifyDataSetChanged();
        });

        // ------------------ //
        // Procedure  Actions //
        // ------------------ //

        if (selectedProcedure.name.equals("equipment_maintenance")) {

            // Creates margin params
            int dpValue = 12; // margin in dips
            float d = context.getResources().getDisplayMetrics().density;
            int margin = (int) (dpValue * d); // margin in pixels
//            LinearLayoutCompat.LayoutParams layoutParams = new LinearLayoutCompat.LayoutParams(
//                    LinearLayoutCompat.LayoutParams.MATCH_PARENT, LinearLayoutCompat.LayoutParams.WRAP_CONTENT);
//            layoutParams.setMargins(margin, margin, margin, margin);

            LinearLayoutCompat linearLayout = new LinearLayoutCompat(context);
            linearLayout.setOrientation(LinearLayoutCompat.VERTICAL);
//            linearLayout.setLayoutParams(layoutParams);
            linearLayout.setPadding(margin, margin, margin, margin);
            linearLayout.setBackgroundResource(R.drawable.border_bottom);

            for (String action : selectedProcedure.optionalActions) {

                // Create checkbox for current action string
                CheckBox checkBox = new CheckBox(context);
                checkBox.setText(Helper.getTranslation(action));
//                checkBox.setLayoutParams(layoutParams);
                checkBox.setSelected(checkedActions.contains(action));
                checkBox.setChecked(checkedActions.contains(action));

                checkBox.setOnClickListener(v -> {
                    v.setSelected(!v.isSelected());
                    Log.i(TAG, "isSelected -> " + v.isSelected());
                    if (v.isSelected())
                        checkedActions.add(action);
                    else
                        checkedActions.remove(action);
                });

                linearLayout.addView(checkBox);
            }

            widgetContainer.addView(linearLayout);

        }

        // ------------ //
        // Group layout //
        // ------------ //

        // Group attributes can only be [target], [input] or [output]

        if (selectedProcedure.group != null) {

            if (BuildConfig.DEBUG)
                Log.i(TAG, "group --> " + selectedProcedure.group);

            View groupView = inflater.inflate(R.layout.widget_with_icon_and_recycler, container, false);
            ImageView icon = groupView.findViewById(R.id.icon);
            TextView label = groupView.findViewById(R.id.widget_label);
            TextView addButton = groupView.findViewById(R.id.widget_add);
            RecyclerView groupRecycler = groupView.findViewById(R.id.widget_recycler);
            groupRecycler.setLayoutManager(new LinearLayoutManager(context));
            groupRecycler.addItemDecoration(new MarginTopItemDecoration(context, 16));
            groupRecycler.setNestedScrollingEnabled(false);

            if (selectedProcedure.group.equals(ZONE)) {

//                icon.setImageResource(getWidgetIcon("land_parcel"));
                icon.setVisibility(View.GONE);
                label.setText(Helper.getTranslation(ZONE));

                ZoneAdapter zoneAdapter = new ZoneAdapter(zoneList, listener);
                groupRecycler.setAdapter(zoneAdapter);

                addButton.setOnClickListener(v -> {
                    zoneList.add(new Zone());
                    zoneAdapter.notifyDataSetChanged();
                });


            } else if (selectedProcedure.group.equals(WORK)) {

                icon.setVisibility(View.GONE);
//                icon.setImageResource(R.drawable.icon_settings);
                label.setText(R.string.tool_maintaining);

                WorkAdapter workAdapter = new WorkAdapter(workList, listener);
                groupRecycler.setAdapter(workAdapter);

                addButton.setOnClickListener(v -> {
                    workList.add(new Work());
                    workAdapter.notifyDataSetChanged();
                });
            }

            groupRecycler.requestLayout();

            // Add view
            widgetContainer.addView(groupView);
        }

        // -------------- //
        // Targets layout //
        // -------------- //

        for (GenericEntity entity : selectedProcedure.target) {
            if (entity.group == null) {
                if (BuildConfig.DEBUG)
                    Log.i(TAG, "target --> " + entity.name);

//                if (!entity.name.equals(EQUIPMENT)) {
                    widgetContainer.addView(new WidgetParamView(context, listener, entity, selectedParamsList, TARGETS));
//                } else {
//
//                    // Get layout
//                    View equipmentTargetView = inflater.inflate(R.layout.widget_with_icon_and_recycler, container, false);
//
//                    ImageView icon = equipmentTargetView.findViewById(R.id.icon);
//                    icon.setImageResource(getWidgetIcon(entity.name));
//
//                    // Set param label
//                    TextView label = equipmentTargetView.findViewById(R.id.widget_label);
//                    label.setText(Helper.getStringId(entity.name));
//
//                    // Add onClick listener
//                    TextView addButton = equipmentTargetView.findViewById(R.id.widget_add);
//                    addButton.setOnClickListener(v -> listener.onFormFragmentInteraction(entity.name, entity.filter, TARGETS));
//
//                    // Initialize Recycler
//                    RecyclerView targetRecycler = equipmentTargetView.findViewById(R.id.widget_recycler);
//                    targetRecycler.setLayoutManager(new LinearLayoutManager(context));
//                    targetRecycler.addItemDecoration(new MarginTopItemDecoration(context, 16));
//
//                    List<GenericItem> equipmentTargets = getCurrentDataset(entity.name, TARGETS);
//                    Log.i(TAG, "Equipment target -> "+ equipmentTargets);
//
//                    // Here quantity is hourCounter
//                    QuantityItemAdapter quantityItemAdapter = new QuantityItemAdapter(equipmentTargets, entity, true, false);
//                    targetRecycler.setAdapter(quantityItemAdapter);
//                    targetRecycler.requestLayout();
//
//                    // Set visibility if one item is corresponding current input variety
//                    int visibility = quantityItemAdapter.getItemCount() == 0 ? View.GONE : View.VISIBLE;
//                    targetRecycler.setVisibility(visibility);
//
//                    // Add view
//                    widgetContainer.addView(equipmentTargetView);
//                }
            }
        }

        // ------------- //
        // Inputs layout //
        // ------------- //

        for (GenericEntity inputType : selectedProcedure.input) {

            if (inputType.group == null) {
                if (BuildConfig.DEBUG)
                    Log.i(TAG, "input --> " + inputType.name);

                // Get layout
                View inputView = inflater.inflate(R.layout.widget_with_icon_and_recycler, container, false);

                ImageView icon = inputView.findViewById(R.id.icon);
                icon.setImageResource(getWidgetIcon(inputType.name));

                // Set warning message if mix not allowed
                TextView warningMessage = inputView.findViewById(R.id.widget_warning_message);

                // Set param label
                TextView label = inputView.findViewById(R.id.widget_label);
                label.setText(Helper.getStringId(inputType.name));

                // Add onClick listener
                TextView addButton = inputView.findViewById(R.id.widget_add);
                addButton.setOnClickListener(v -> listener.onFormFragmentInteraction(inputType.name, inputType.filter, INPUTS));

                // Initialize Recycler
                RecyclerView inputRecycler = inputView.findViewById(R.id.widget_recycler);
                inputRecycler.setLayoutManager(new LinearLayoutManager(context));
                inputRecycler.addItemDecoration(new MarginTopItemDecoration(context, 16));
                inputRecycler.setNestedScrollingEnabled(false);
                int visibility;
                if (inputType.name.equals("plant_medicine")) {

                    updatePhytosanitaryList();

                    // Phytosanitary specific adapter
                    phytoAdapter = new PhytosanitaryProductAdapter(
                            getCurrentDataset(inputType.name, INPUTS), inputType, warningMessage);
                    inputRecycler.setAdapter(phytoAdapter);
                    visibility = phytoAdapter.getItemCount() == 0 ? View.GONE : View.VISIBLE;
                    if (phytoAdapter.getItemCount() >= 2)
                        verifyMix(warningMessage);
                } else {
                    QuantityItemAdapter quantityItemAdapter = new QuantityItemAdapter(
                            getCurrentDataset(inputType.name, INPUTS), inputType, false, false);
                    inputRecycler.setAdapter(quantityItemAdapter);
                    visibility = quantityItemAdapter.getItemCount() == 0 ? View.GONE : View.VISIBLE;
                }

                // Set visibility if one item is corresponding current input variety
                inputRecycler.setVisibility(visibility);

                // Add view
                widgetContainer.addView(inputView);
            }
        }

        // ------------- //
        // Output layout //
        // ------------- //

        for (GenericEntity outputType : selectedProcedure.output) {

            if (outputType.group == null) {
                if (BuildConfig.DEBUG) Log.i(TAG, "Build layout for output --> " + outputType.name);

                // Get layout (same as input)
                View outputView = inflater.inflate(R.layout.widget_with_icon_and_recycler, container, false);

                ImageView icon = outputView.findViewById(R.id.icon);
                icon.setImageResource(getWidgetIcon(outputType.name));

                // Set param label
                TextView label = outputView.findViewById(R.id.widget_label);
                label.setText(Helper.getStringId(outputType.name));

                // Add onClick listener
                TextView addButton = outputView.findViewById(R.id.widget_add);
                addButton.setOnClickListener(v -> listener.onFormFragmentInteraction(outputType.name, outputType.filter, OUTPUTS));

                // Initialize Recycler
                RecyclerView outputRecycler = outputView.findViewById(R.id.widget_recycler);
                outputRecycler.setLayoutManager(new LinearLayoutManager(context));
                outputRecycler.addItemDecoration(new MarginTopItemDecoration(context, 16));
                outputRecycler.setNestedScrollingEnabled(false);
                QuantityItemAdapter outputItemAdapter = new QuantityItemAdapter(
                        getCurrentDataset(outputType.name, OUTPUTS), outputType, false, true);
                outputRecycler.setAdapter(outputItemAdapter);
                outputRecycler.requestLayout();

                // Set visibility if one item is corresponding current input variety
                int visibility = outputItemAdapter.getItemCount() == 0 ? View.GONE : View.VISIBLE;
                outputRecycler.setVisibility(visibility);

                outputItemAdapter.notifyDataSetChanged();

                // Add view
                widgetContainer.addView(outputView);
            }
        }

        // -------------- //
        // Workers layout //
        // -------------- //

        for (GenericEntity entity : selectedProcedure.doer) {
            if (BuildConfig.DEBUG) Log.i(TAG, "doer --> " + entity.name);
            widgetContainer.addView(new WidgetParamView(context, listener, entity, selectedParamsList, WORKERS));
        }

        // ----------------- //
        // Equipments layout //
        // ----------------- //

        for (GenericEntity entity : selectedProcedure.tool) {
            if (BuildConfig.DEBUG) Log.i(TAG, "tool --> " + entity.name);

//            final List<String> equipmentsWithHourCounter = Arrays.asList(TRACTOR, EQUIPMENT, TOOL, CROPPER);
//            if (!equipmentsWithHourCounter.contains(entity.name)) {
//                widgetContainer.addView(new WidgetParamView(context, listener, entity, selectedParamsList, "equipments"));
//            } else {

            // Get layout
            View equipmentView = inflater.inflate(R.layout.widget_with_icon_and_recycler, container, false);

            ImageView icon = equipmentView.findViewById(R.id.icon);
            icon.setImageResource(getWidgetIcon(entity.name));

            // Set param label
            TextView label = equipmentView.findViewById(R.id.widget_label);
            label.setText(Helper.getStringId(entity.name));

            // Add onClick listener
            TextView addButton = equipmentView.findViewById(R.id.widget_add);
            addButton.setOnClickListener(v -> listener.onFormFragmentInteraction(entity.name, entity.filter, EQUIPMENTS));

            // Initialize Recycler
            RecyclerView inputRecycler = equipmentView.findViewById(R.id.widget_recycler);
            inputRecycler.setLayoutManager(new LinearLayoutManager(context));
            inputRecycler.addItemDecoration(new MarginTopItemDecoration(context, 16));
            inputRecycler.setNestedScrollingEnabled(false);
            QuantityItemAdapter quantityItemAdapter = new QuantityItemAdapter(
                    getCurrentDataset(entity.name, EQUIPMENTS), entity, true, false);
            inputRecycler.setAdapter(quantityItemAdapter);
            inputRecycler.requestLayout();

            // Set visibility if one item is corresponding current input variety
            int visibility = quantityItemAdapter.getItemCount() == 0 ? View.GONE : View.VISIBLE;
            inputRecycler.setVisibility(visibility);

            // Add view
            widgetContainer.addView(equipmentView);
        }

        // Add description input field
        View descView = inflater.inflate(R.layout.widget_simple_text_input, container, false);
        TextInputEditText descInput = descView.findViewById(R.id.description);
        widgetContainer.addView(descView);
        descInput.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
            @Override public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
            @Override public void afterTextChanged(Editable editable) {
                if (editable != null && !editable.toString().equals(""))
                    description = editable.toString();
            }
        });

        // Set ScrollView to previous position
        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        //    scrollView.setOnScrollChangeListener((v, scrollX, scrollY, oldScrollX, oldScrollY) -> scrollPosition = scrollY);
        scrollView.scrollTo(0, scrollPosition);

        return view;
    }

    private static void updatePhytosanitaryList() {
        selectedPhytosanitaryList = new ArrayList<>();
        for (GenericItem item : selectedParamsList) {
            if (item.refId != null && item.referenceName.containsKey("plant_medicine"))
                selectedPhytosanitaryList.add(getPhytosanitaryReferenceFromDB(item.refId));
        }
    }

    public static void verifyMix(View warningView) {
        if (BuildConfig.DEBUG)
            Log.i(TAG, "verifyMix()");
        int visibility = View.GONE;

        updatePhytosanitaryList();

        if (selectedPhytosanitaryList.size() >= 2) {

            for (PhytosanitaryDetails phyto_1 : selectedPhytosanitaryList) {
                for (PhytosanitaryDetails phyto_2 : selectedPhytosanitaryList) {

                    if (phyto_1.id != phyto_2.id) {
                        if ((phyto_1.mixCode == 5 || phyto_2.mixCode == 5)
                                || (phyto_1.mixCode == 4 && phyto_2.mixCode == 4)
                                || (phyto_1.mixCode == 3 && phyto_2.mixCode == 3)
                                || (phyto_1.mixCode == 2 && phyto_2.mixCode == 2)) {
                            Log.e(TAG, "Phytosanitary mix not allowed");
                            visibility = View.VISIBLE;
                        }
                    } else if (_getZNTMax(phyto_1.usages) >= 100
                            || _getZNTMax(phyto_2.usages) >= 100)
                        visibility = View.VISIBLE;
                }
            }
        }
        warningView.setVisibility(visibility);
    }

    private static int _getZNTMax(List<PhytosanitaryUsage> usages) {
        int znt = 0;
        if (usages != null && !usages.isEmpty())
            for (PhytosanitaryUsage usage : usages)
                if (usage.aquaticBuffer != null)
                    znt = Math.max(usage.aquaticBuffer, znt);
        return znt;
    }

    private List<GenericItem> getCurrentDataset(String type, String role) {

        List<GenericItem> dataset = new ArrayList<>();
        List<GenericItem> items = role.equals(OUTPUTS) ? selectedVariantsList : selectedParamsList;

        // Loop over all items availables for this role
        for (GenericItem item : items)
            if (item.referenceName.containsKey(type))
                dataset.add(item);

        return dataset;
    }

    public static List<List<String>> getSelectedCrops() {
        List<List<String>> result = new ArrayList<>();

        if (selectedProcedure.group == null) {
            for (GenericItem item : selectedParamsList)
                if (item.referenceName.containsKey("cultivation") || item.referenceName.containsKey("plant"))
                    if (!item.variety.equals("land_parcel"))
                        result.add(Ontology.findPlantTreeRealm(item.variety));
        } else {
            for (Zone zone : zoneList)
                if (zone.plant != null)
                    result.add(Ontology.findPlantTreeRealm(zone.plant.variety));
        }
        return result;
    }

    public static BigDecimal getSelectedArea() {
        BigDecimal area = BigDecimal.ZERO;
        if (selectedProcedure.group == null) {
            for (GenericItem item : selectedParamsList)
                if (item.referenceName.containsKey("cultivation")
                        || item.referenceName.containsKey("plant")) {

                    if (item.netSurfaceArea.contains("ha")) {
                        String itemArea = item.netSurfaceArea.split(" ")[0].replace(",", ".");
                        area = area.add(new BigDecimal(itemArea));
                    }
                }
        } else {
            for (Zone zone : zoneList)
                if (zone.landParcel != null) {
                    String itemArea = zone.landParcel.netSurfaceArea.split(" ")[0].replace(",", ".");
                    area = area.add(new BigDecimal(itemArea));
                }
        }
        return area;
    }

    public static void refreshPhytoAdapter() {
        if (phytoAdapter != null) {
            if (BuildConfig.DEBUG)
                Log.d(TAG, "refreshPhytoAdapter()");
            phytoAdapter.notifyDataSetChanged();
        }
    }

//    private void chipGroupDisplay(ChipGroup group) {
//        boolean itemCounter = false;
//        for (GenericItem item : paramsList)
//            if ((item.variety.equals(PLANT) || item.variety.equals(LAND_PARCEL)) && item.isSelected) {
//                itemCounter = true;
//                break;
//            }
//        group.setVisibility(itemCounter ? View.VISIBLE : View.GONE);
//    }

    public static PhytosanitaryDetails getPhytoRefFromId(int id) {
        for (PhytosanitaryDetails phyto : selectedPhytosanitaryList)
            if (phyto.id == id)
                return phyto;
        return null;
    }

    private static PhytosanitaryDetails getPhytosanitaryReferenceFromDB(int id) {
        if (BuildConfig.DEBUG)
            Log.d(TAG, "getPhytosanitaryReferenceFromDB");

        List<PhytosanitaryUsage> usages = new ArrayList<>();
        PhytosanitaryDetails phytoDetails = null;
        ContentResolver cr = Zero.getContext().getContentResolver();

        // Get usages
        try (Cursor curs = cr.query(PhytosanitaryUsages.CONTENT_URI, PhytosanitaryUsages.PROJECTION_ALL,
                String.format("product_id = %s", id), null, PhytosanitaryUsages.ORDER_BY_NAME)) {

            if (curs != null && curs.getCount() > 0) {
                while (curs.moveToNext()) {
                    PhytosanitaryUsage usage = getPhytosanitaryUsage(curs);
//                    usage.ephyCropsets = getEphyCropset(cr, usage.species);
                    usage.doseUnitDimension = getUnitDimension(usage.doseUnit);
                    if (usage.species.startsWith("ephy_"))
                        usage.ephyCropsets = getEphyCropset(cr, usage.species);
                    usages.add(usage);
                }
            }
        }

        // Get phyto
        try (Cursor curs = cr.query(PhytosanitaryReferences.CONTENT_URI, PhytosanitaryReferences.PROJECTION_ALL,
                String.format("id = %s", id), null, null)) {

            if (curs != null && curs.moveToFirst()) {
                phytoDetails = getPhytosanitaryDetails(curs);
                phytoDetails.usages = usages;
            }
        }

        return phytoDetails;
    }

    private static String getUnitDimension(String unit) {
        unit = unit != null ? unit : "no_dim";

        switch (unit) {
            case "liter_per_hectare":
            case "liter_per_square_meter":
                return "volume_area_density";

            case "kilogram_per_square_meter":
            case "gram_per_square_meter":
                return "mass_area_density";

            default:
                return null;
        }
    }

    private static String getEphyCropset(ContentResolver cr, String specie) {

        try (Cursor curs = cr.query(EphyCropsets.CONTENT_URI, EphyCropsets.PROJECTION_CROPSETS,
                String.format("name = '%s'", specie), null, null)) {

            if (curs != null && curs.moveToFirst())
                return curs.getString(0);
            else
                return null;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public static int getWidgetIcon(String refName) {

        List<String> targets = Arrays.asList("plant", "land_parcel", "cultivation", "zone");
        List<String> drivers = Arrays.asList("driver", "forager_driver");
        List<String> doers =  Arrays.asList("caregiver", "doer", "worker", "operator", "inseminator", "responsible", "wine_man", "mechanic");
        List<String> inputs =  Arrays.asList("animal_food", "animal_medicine", "bottles",
                "cleaner_product", "construction_material", "consumable_part", "consumable_part",
                "corks", "disinfectant", "energy", "fertilizer", "food", "fuel", "herbicide",
                "item", "litter", "mulching_material", "oenological_intrant", "oil",
                "packaging_material", "plant_medicine", "plants", "pollinating_insects",
                "product_to_prepare", "product_to_sort", "protective_canvas", "protective_net",
                "replacement_part", "seedlings", "seeds", "stakes", "straw_to_bunch", "tunnel",
                "vial", "water", "wine", "wire_fence");

        if (refName.equals("tractor"))
            return R.drawable.icon_tractor;
        if (targets.contains(refName))
            return R.drawable.icon_field;
        if (drivers.contains(refName))
            return R.drawable.icon_driver;
        if (doers.contains(refName))
            return R.drawable.icon_doer;
        if (inputs.contains(refName))
            return R.drawable.icon_sack;

        return R.drawable.icon_trailer;
    }

    private void setDriverIfExists() {

        for (GenericEntity doer : selectedProcedure.doer) {
            if (doer.name.equals("driver")) {

                try (Cursor cursor = context.getContentResolver().query(
                        ZeroContract.Products.CONTENT_URI, ZeroContract.Products.PROJECTION,
                        "ek_id=? AND user=?", new String[]{
                                String.valueOf(workerId),
                                AccountTool.getAccountInstance(account, context)},
                        null)) {

                    while (cursor != null && cursor.moveToNext()) {
                        GenericItem item = new GenericItem();
                        item.id = cursor.getInt(0);
                        item.name = cursor.getString(1);
                        item.number = cursor.getString(2);
                        item.workNumber = cursor.getString(3);
                        item.variety = cursor.getString(4);
                        item.abilities = cursor.getString(5).split(",");
                        item.population = cursor.getString(6);
                        item.containerName = cursor.getString(8);
                        item.netSurfaceArea = cursor.getString(9);
                        item.born_at = cursor.isNull(10) ? null : new Date(cursor.getLong(10));
                        item.dead_at = cursor.isNull(11) ? null : new Date(cursor.getLong(11));
//                        item.production_started_at = cursor.isNull(10) ? null : new Date(cursor.getLong(10));
//                        item.production_stopped_at = cursor.isNull(11) ? null : new Date(cursor.getLong(11));
                        item.referenceName.put("driver", "workers");
                        selectedParamsList.add(item);
                    }
                }
            }
        }
    }

    public interface OnFragmentInteractionListener {
        void onFormFragmentInteraction(String fragmentTag, String filter, String role);
    }

}


