package ekylibre.zero.intervention;

import android.accounts.Account;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.preference.PreferenceManager;

import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
//import com.google.zxing.integration.android.IntentIntegrator;
//import com.google.zxing.integration.android.IntentResult;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import ekylibre.database.ZeroContract.Products;
import ekylibre.database.ZeroContract.Interventions;
import ekylibre.database.ZeroContract.InterventionParameters;
import ekylibre.database.ZeroContract.DetailedInterventions;
import ekylibre.database.ZeroContract.DetailedInterventionAttributes;
import ekylibre.database.ZeroContract.WorkingPeriods;
import ekylibre.database.ZeroContract.WorkingPeriodAttributes;
import ekylibre.database.ZeroContract.Crumbs;

import ekylibre.util.AccountTool;
import ekylibre.util.DateConstant;
import ekylibre.util.ExtendedChronometer;
import ekylibre.util.Helper;
import ekylibre.util.PermissionManager;
import ekylibre.util.UpdatableActivity;
import ekylibre.zero.BuildConfig;
import ekylibre.zero.R;
import ekylibre.zero.inter.model.GenericItem;

import static ekylibre.util.Helper.toIso8601Android;
import static ekylibre.zero.home.MainActivity.CHANNEL_ID;
import static ekylibre.zero.home.Zero.LARRERE;
import static ekylibre.zero.inter.InterActivity.FINISHED;


public class InterventionActivity extends UpdatableActivity implements TrackingListenerWriter {

    private final String TAG = "InterventionActivity";

    /* ****************************
     **      CONSTANT VALUES
     ** ***************************/
    protected final int PRODUCT_NAME = 1;
    protected final int LABEL = 2;
    protected final int NAME = 3;
    protected final int PRODUCT_ID = 4;
    protected final int HOUR_METER = 5;
    protected final String PREPARATION = "preparation";
    protected final String TRAVEL = "travel";
    protected final String INTERVENTION = "intervention";
    protected final String PAUSE = "pause";
    public final static String NEW = "new_intervention";
    public static final String _interventionID = "intervention_id";
    public final static String STATUS_PAUSE = "PAUSE";
    public final static String STATUS_FINISHED = "FINISHED";
    public final static String STATUS_IN_PROGRESS = "IN_PROGRESS";
    public final static String DETAILED = "DETAILED";

    /* ****************************
     ** Notification builder and variables
     ** ***************************/
    private NotificationManagerCompat mNotificationManager;
    private NotificationCompat.Builder mNotificationBuilder;
    private int mNotificationID;

    /* ****************************
     ** Interface elements variables
     ** ***************************/
    private ScrollView infoScroll;
    private RelativeLayout layoutActivePreparation, layoutActiveTravel, layoutActiveIntervention, layoutPreparation, layoutTravel, layoutIntervention;
    private View preparation, travel, intervention;
    private ExtendedChronometer chronoGeneral, chronoPreparation, chronoTravel, chronoIntervention, chronoActivePreparation, chronoActiveTravel, chronoActiveIntervention;
    private CheckBox diffStuff;
    private Button mStartButton, mMapButton;
    private TextView mProcedureNature;
    private CardView cardView;
    private TextView gpsAccuracyMessage;

    /* ****************************
     **      Class variables
     ** ***************************/
    private long detailedInterventionID = -1;
    private int mInterventionID;
    protected String state;
    private String mLastProcedureNature, mLastProcedureNatureName;
    private String mLocationProvider;
    private TrackingListener mTrackingListener;
    private Account mAccount;
    private AlertDialog.Builder mProcedureChooser;
    private SharedPreferences mPreferences;
//    private IntentIntegrator mScanIntegrator;
    private RelativeLayout infoLayout;
    private LocationManager mLocationManager;
    private CrumbsCalculator crumbsCalculator;
    private boolean mNewIntervention;
    private String started_at = null;
    private String stopped_at = null;
    SimpleDateFormat dateFormatter = new SimpleDateFormat(DateConstant.ISO_8601);
    private boolean inProgress = false;
    private boolean newIntervStarted = false;
    private boolean safeDestroy = false;
    private ContentResolver cr;
    private List<GenericItem> tractors;
    private AlertDialog hourMeterDialog;
    private boolean hasHourMeter = false;
    private String description;
    private TextInputLayout descriptionView;

    @Override
    public void onStart() {
        super.onStart();
        if (!AccountTool.isAnyAccountExist(this))
            AccountTool.askForAccount(this, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.intervention, menu);
        return (super.onCreateOptionsMenu(menu));
    }

    /*
     ** Actions on toolbar items
     ** Items are identified by their view id
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_save) {
            saveAndExit();
        } else if (id == android.R.id.home) {
            exitInterface();
            return true;
        } else if (id == R.id.action_map) {
            openMap(null);
        }
        return (super.onOptionsItemSelected(item));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAccount = AccountTool.getCurrentAccount(this);
        cr = getContentResolver();
        tractors = new ArrayList<>();

        // Set content view
        setContentView(R.layout.intervention);

        super.setToolBar();
        mNewIntervention = getIntent().getBooleanExtra(InterventionActivity.NEW, false);

        layoutPreparation = findViewById(R.id.layout_preparation);
        layoutTravel = findViewById(R.id.layout_travel);
        layoutIntervention = findViewById(R.id.layout_intervention);
        layoutActivePreparation = findViewById(R.id.layout_active_preparation);
        layoutActiveTravel = findViewById(R.id.layout_active_travel);
        layoutActiveIntervention = findViewById(R.id.layout_active_intervention);
        preparation = findViewById(R.id.preparation);
        travel = findViewById(R.id.travel);
        intervention = findViewById(R.id.intervention);
        chronoPreparation = findViewById(R.id.chrono_preparation);
        chronoTravel = findViewById(R.id.chrono_travel);
        chronoIntervention = findViewById(R.id.chrono_intervention);
        chronoActivePreparation = findViewById(R.id.chrono_active_preparation);
        chronoActiveTravel = findViewById(R.id.chrono_active_travel);
        chronoActiveIntervention = findViewById(R.id.chrono_active_intervention);
        chronoGeneral = findViewById(R.id.chrono_general);
        mProcedureNature = findViewById(R.id.procedure_nature);
        mStartButton = findViewById(R.id.start_intervention_button);
        mMapButton = findViewById(R.id.map_button);
        infoLayout = findViewById(R.id.infoLayout);
        infoScroll = findViewById(R.id.interventionInfo);
        cardView = findViewById(R.id.card_view);
        gpsAccuracyMessage = findViewById(R.id.gps_accuracy_message);
        diffStuff = findViewById(R.id.diff_stuff);
        diffStuff.setOnClickListener(view -> {
            CheckBox box = (CheckBox) view;
            Log.d(TAG, "CheckBox state => " + box.isChecked());
        });

        descriptionView = findViewById(R.id.descriptionInput);
        if (detailedInterventionID != -1)
            descriptionView.setVisibility(View.GONE);
        else {
            EditText descriptionEditText = descriptionView.getEditText();
            if (descriptionEditText != null) {

                descriptionEditText.addTextChangedListener(new TextWatcher() {
                    @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
                    @Override public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
                    @Override public void afterTextChanged(Editable editable) {
                        if (!editable.toString().isEmpty())
                            description = editable.toString();
                    }
                });
            }
        }

        // Acquire a reference to the system Location Manager
        mTrackingListener = new TrackingListener(this);
        mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        mLocationProvider = LocationManager.GPS_PROVIDER;

        mPreferences = PreferenceManager.getDefaultSharedPreferences(this);

//        mScanIntegrator = new IntentIntegrator(this);

//        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mNotificationManager = NotificationManagerCompat.from(this);

//        createNotificationChannel(mNotificationManager);
        mNotificationID = 1;
        mNotificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setOngoing(true)
                .setContentTitle(getString(R.string.app_name))
                .setContentText("")
                .setContentIntent(PendingIntent.getActivity(this, 0, new Intent(this, InterventionActivity.class), PendingIntent.FLAG_UPDATE_CURRENT))
                .setSmallIcon(R.mipmap.ic_stat_notify);

        if (!mNewIntervention)
            prepareRequest();
        else {
            createProcedureChooser();
            prepareSimpleIntervention();
        }
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.close);

        // Warn about GPS status
        if (!mLocationManager.isProviderEnabled(mLocationProvider))
            gpsAccuracyMessage.setText(R.string.gps_disabled);
        else if (!PermissionManager.GPSPermissions(this, this))
            gpsAccuracyMessage.setText(R.string.gps_tracking_disabled);

//        // Add description input field
//        View descView = getLayoutInflater().inflate(R.layout.widget_simple_text_input, null);
//        TextInputEditText descInput = descView.findViewById(R.id.description);
//        infoLayout.addView(descInput);
//        descInput.addTextChangedListener(new TextWatcher() {
//            @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
//            @Override public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
//            @Override public void afterTextChanged(Editable editable) {
//                if (editable != null && !editable.toString().equals(""))
//                    description = editable.toString();
//            }
//        });
    }

//    // TODO -> not working yet (alert message is here)
//    private void createNotificationChannel(NotificationManager notificationManager) {
//        // Create the NotificationChannel, but only on API 26+ because
//        // the NotificationChannel class is new and not in the support library
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            CharSequence name = getString(R.string.channel_name);
//            String description = getString(R.string.channel_description);
//            int importance = NotificationManager.IMPORTANCE_DEFAULT;
//            NotificationChannel channel = new NotificationChannel("CHANNEL_ID", name, importance);
//            channel.setDescription(description);
//            // Register the channel with the system; you can't change the importance
//            // or other notification behaviors after this
////            NotificationManager notificationManager = getSystemService(NotificationManager.class);
//            notificationManager.createNotificationChannel(channel);
//        }
//    }

    private void setInProgressState() {
        if (!inProgress) {
            ContentValues cv = new ContentValues();
            cv.put(Interventions.STATE, STATUS_IN_PROGRESS);
            cr.update(Interventions.CONTENT_URI, cv,
                    Interventions._ID + " == " + mInterventionID, null);
        }
        inProgress = true;
    }

    private void prepareSimpleIntervention() {
        cardView.setVisibility(View.INVISIBLE);
        diffStuff.setVisibility(View.GONE);
        chronoGeneral.setVisibility(View.GONE);
        layoutPreparation.setVisibility(View.GONE);
        layoutTravel.setVisibility(View.GONE);
        layoutIntervention.setVisibility(View.GONE);
        mStartButton.setVisibility(View.VISIBLE);
    }

    private void prepareRequest() {
        mStartButton.setVisibility(View.GONE);
        Intent intent = getIntent();
        mInterventionID = intent.getIntExtra(Interventions._ID, 0);
        Log.e(TAG, "current intervention ID = " + mInterventionID);
        // Get detailed intervention ID if exists
//        detailedInterventionID = intent.getLongExtra("detailed_intervention_id", 0);
//        detailedProcedureName = intent.getStringExtra("detailed_procedure_name");
        Cursor curs = setBasicValues();
        writeInterventionInfo();
        setPausedValues(curs);
        setNotification();
    }

    private Cursor setBasicValues() {
        Cursor curs = cr.query(
                Interventions.CONTENT_URI,
                Interventions.PROJECTION_PAUSED,
                Interventions._ID + " == " + mInterventionID, null, null);
        if (curs == null || curs.getCount() == 0)
            return (null);
        curs.moveToFirst();

//        if (!curs.isNull(0) && curs.getString(0).equals("PAUSE"))
//            wasInPause = true;

        if (!curs.isNull(8)) {
            detailedInterventionID = curs.getLong(8);
            mLastProcedureNatureName = Helper.getTranslation(curs.getString(7));
        } else {
            mLastProcedureNatureName = curs.getString(6);
        }
        if (!curs.isNull(9) && !curs.getString(9).isEmpty()) {
            description = curs.getString(9);
            descriptionView.getEditText().setText(description);
        }
        setTitle(mLastProcedureNatureName);
        crumbsCalculator = new CrumbsCalculator(mLastProcedureNature);
        diffStuff.setChecked(curs.getInt(1) != 0);
        return (curs);
    }

    private void setPausedValues(Cursor curs) {
        if (curs == null || curs.getString(0) == null || !curs.getString(0).equals("PAUSE"))
            return;
        chronoGeneral.setTime(curs.getInt(2));
        chronoPreparation.setTime(curs.getInt(3));
        chronoActivePreparation.setTime(chronoPreparation.getTime());
        chronoTravel.setTime(curs.getInt(4));
        chronoActiveTravel.setTime(chronoTravel.getTime());
        chronoIntervention.setTime(curs.getInt(5));
        chronoActiveIntervention.setTime(chronoIntervention.getTime());
        curs.close();
    }

    private void writeInterventionInfo() {
        int botId = writeTarget();
        botId = writeInput(botId);
        writeTool(botId);
    }

    private int writeTarget() {
        Cursor cursTarget = queryTarget();
        int botId = View.NO_ID;
        if (cursTarget != null && cursTarget.getCount() > 0) {
            botId = writeCursor(cursTarget, botId, false);
            cursTarget.close();
            return botId;
        }
        return botId;
    }

    private int writeInput(int botId) {
        Cursor cursInput = queryInput();

        if (cursInput == null || cursInput.getCount() == 0)
            return (botId);
        botId = writeCursor(cursInput, botId, false);
        cursInput.close();
        return (botId);
    }

    private void writeTool(int botId) {
        Cursor cursTool = queryTool();

        if (cursTool != null && cursTool.getCount() > 0) {
            writeCursor(cursTool, botId, true);
            cursTool.close();
        }
    }

    private int writeCursor(Cursor curs, int botId, boolean isTool) {

        StringBuilder sb = new StringBuilder();

        while (curs.moveToNext()) {
            if (!curs.getString(NAME).isEmpty()) {

                // Save tool reference if hour counter
//                if (curs.getString(NAME).equals("tractor")
//                        || curs.getString(NAME).equals("equipment")) {

                if (isTool) {

                    try (Cursor hourCurs = cr.query(Products.CONTENT_URI, Products.PROJECTION_HOUR_COUNTER,
                            Products.USER + " =? AND " + Products.EK_ID + "=?",
                            new String[]{
                                    AccountTool.getAccountInstance(mAccount, this),
                                    String.valueOf(curs.getInt(PRODUCT_ID))
                            }, null)) {

                        Log.i(TAG, "hour meter request");

                        while (hourCurs != null && hourCurs.moveToNext()) {
                            if (hourCurs.getInt(0) == 1) {
                                Log.i(TAG, "hasHourMeter");
                                hasHourMeter = true;
                                GenericItem tractor = new GenericItem();
                                tractor.name = curs.getString(PRODUCT_NAME);
                                tractor.id = curs.getInt(PRODUCT_ID);
                                tractor.hourMeter = curs.isNull(HOUR_METER) ? null : new BigDecimal(curs.getFloat(HOUR_METER));
                                tractor.hasHourMeter = true;
                                tractors.add(tractor);
                            }
                        }
                    }
                }
//                }

                if (!sb.toString().isEmpty()) {
                    botId = flushBlock(sb.toString(), botId);
                    sb = new StringBuilder();
                }
                botId = createTitle(curs, botId);
//                titleRef = curs.getString(NAME);
            }
            sb.append(curs.getString(PRODUCT_NAME));
            int quantityIndex = curs.getColumnIndex(DetailedInterventionAttributes.QUANTITY_VALUE);
            int unitIndex = curs.getColumnIndex(DetailedInterventionAttributes.QUANTITY_UNIT);
            if (quantityIndex != -1) {
                sb.append("\n").append(curs.getFloat(quantityIndex))
                        .append(" ").append(Helper.getTranslation(curs.getString(unitIndex)));
            }
//            sb.append("\n");
        }
        botId = flushBlock(sb.toString(), botId);
        return (botId);
    }

    /**
     * Create header line for intervention parameters
     */
    private int createTitle(Cursor curs, int id) {
        TextView title = new TextView(this);
        StringBuilder sb = new StringBuilder();
        sb.append("• ");

        if (detailedInterventionID == -1)
            sb.append(curs.getString(LABEL));
        else
            sb.append(Helper.getTranslation(curs.getString(LABEL)));

        title.setText(sb.toString());
        title.setTextColor(Color.BLACK);
        title.setTypeface(null, Typeface.BOLD);
        title.setId(View.generateViewId());
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.addRule(RelativeLayout.BELOW, id);

        title.setLayoutParams(lp);
        title.setTextSize(18);
        infoLayout.addView(title);
        return (title.getId());
    }

    private int flushBlock(String str, int id) {
        TextView text = new TextView(this);

        text.setText(str);
        text.setId(View.generateViewId());
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.setMarginStart(20);
        lp.addRule(RelativeLayout.BELOW, id);

        text.setLayoutParams(lp);
        text.setGravity(Gravity.LEFT);
        text.setTextColor(Color.parseColor("#454545"));
        text.setTextSize(15);

        infoLayout.addView(text);
        return (text.getId());
    }

    private Cursor queryTarget() {

        if (detailedInterventionID == -1)
            return cr.query(
                    InterventionParameters.CONTENT_URI,
                    InterventionParameters.PROJECTION_TARGET_FULL,
                    InterventionParameters.ROLE + " LIKE " + "\"target\"" + " AND "
                            + mInterventionID + " == " + InterventionParameters.FK_INTERVENTION,
                    null,
                    InterventionParameters.NAME);
        else
            return cr.query(
                    DetailedInterventionAttributes.CONTENT_URI,
                    DetailedInterventionAttributes.PROJECTION_ZERO,
                    DetailedInterventionAttributes.ROLE + " LIKE ? AND " +
                    DetailedInterventionAttributes.DETAILED_INTERVENTION_ID + "=?",
                    new String[] {"targets", String.valueOf(detailedInterventionID)},
                    null);
    }

    private Cursor queryInput() {

        if (detailedInterventionID == -1)
            return cr.query(InterventionParameters.CONTENT_URI,
                    InterventionParameters.PROJECTION_INPUT_FULL,
                    InterventionParameters.ROLE + " LIKE \"input\" AND "
                            + mInterventionID + " == " + InterventionParameters.FK_INTERVENTION,
                    null, InterventionParameters.NAME);
        else
            return cr.query(DetailedInterventionAttributes.CONTENT_URI,
                    DetailedInterventionAttributes.PROJECTION_ZERO_WITH_QUANTITY,
                    DetailedInterventionAttributes.ROLE + " LIKE ? AND " +
                            DetailedInterventionAttributes.DETAILED_INTERVENTION_ID + "=?",
                    new String[] {"inputs", String.valueOf(detailedInterventionID)},
                    null);
    }

    private Cursor queryTool() {
        // Planned intervention
        if (detailedInterventionID == -1)
            return cr.query(InterventionParameters.CONTENT_URI,
                    InterventionParameters.PROJECTION_TOOL_FULL,
                    InterventionParameters.ROLE + " LIKE \"tool\" AND "
                            + mInterventionID + " == " + InterventionParameters.FK_INTERVENTION,
                    null, InterventionParameters.NAME);

        // Detailed intervention
        else {
            Log.i(TAG, "queryTool with detailedInterventionID");
            return cr.query(DetailedInterventionAttributes.CONTENT_URI,
                    DetailedInterventionAttributes.PROJECTION_ZERO,
                    DetailedInterventionAttributes.ROLE + " LIKE ? AND " +
                            DetailedInterventionAttributes.DETAILED_INTERVENTION_ID + "=?",
                    new String[]{"equipments", String.valueOf(detailedInterventionID)},
                    null);
        }
    }

    private void updateWorkingPeriods(String currentState) {
        setInProgressState();
        if (started_at == null) {
            Calendar cal = Calendar.getInstance();
            started_at = toIso8601Android(cal.getTime());
        } else if (currentState.equals(PAUSE)) {
            Calendar cal = Calendar.getInstance();
            stopped_at = toIso8601Android(cal.getTime());
            addWorkingPeriodsToLocalDatabase();
            started_at = null;
        } else {
            Calendar cal = Calendar.getInstance();
            stopped_at = toIso8601Android(cal.getTime());
            addWorkingPeriodsToLocalDatabase();
            started_at = stopped_at;
        }
    }

    private void addWorkingPeriodsToLocalDatabase() {

        ContentValues values = new ContentValues();
        values.put(WorkingPeriods.NATURE, state);
        values.put(WorkingPeriods.STARTED_AT, started_at);
        values.put(WorkingPeriods.STOPPED_AT, stopped_at);

        Uri table;
        if (detailedInterventionID == -1) {
            values.put(WorkingPeriods.FK_INTERVENTION, mInterventionID);
            table = WorkingPeriods.CONTENT_URI;
        } else {
            values.put(WorkingPeriodAttributes.DETAILED_INTERVENTION_ID, detailedInterventionID);
            table = WorkingPeriodAttributes.CONTENT_URI;
        }

        cr.insert(table, values);
    }

    public void phasePreparation(View view) {
        updateWorkingPeriods(PREPARATION);

        chronoGeneral.startTimer();

        state = PREPARATION;
        Log.d(TAG, "Activating preparation !  State => " + state);
        layoutPreparation.setVisibility(View.GONE);
        layoutActivePreparation.setVisibility(View.VISIBLE);
        layoutActiveTravel.setVisibility(View.GONE);
        layoutActiveIntervention.setVisibility(View.GONE);
        layoutTravel.setVisibility(View.VISIBLE);
        layoutIntervention.setVisibility(View.VISIBLE);

        chronoActivePreparation.startTimer();
        chronoActiveTravel.stopTimer();
        chronoActiveIntervention.stopTimer();
        chronoTravel.setTime(chronoActiveTravel.getTime());
        chronoIntervention.setTime(chronoActiveIntervention.getTime());

    }

    public void phaseTravel(View view) {
        updateWorkingPeriods(TRAVEL);

        chronoGeneral.startTimer();

        state = TRAVEL;
        Log.d(TAG, "Activating travel !  State => " + state);
        layoutTravel.setVisibility(View.GONE);
        layoutActiveTravel.setVisibility(View.VISIBLE);
        layoutActivePreparation.setVisibility(View.GONE);
        layoutActiveIntervention.setVisibility(View.GONE);
        layoutPreparation.setVisibility(View.VISIBLE);
        layoutIntervention.setVisibility(View.VISIBLE);

        chronoActiveTravel.startTimer();
        chronoActivePreparation.stopTimer();
        chronoActiveIntervention.stopTimer();
        chronoPreparation.setTime(chronoActivePreparation.getTime());
        chronoIntervention.setTime(chronoActiveIntervention.getTime());
        startTracking();

    }

    public void phaseIntervention(View view) {
        updateWorkingPeriods(INTERVENTION);

        chronoGeneral.startTimer();

        state = INTERVENTION;
        Log.d(TAG, "Activating intervention !  State => " + state);
        layoutIntervention.setVisibility(View.GONE);
        layoutActiveIntervention.setVisibility(View.VISIBLE);
        layoutActivePreparation.setVisibility(View.GONE);
        layoutActiveTravel.setVisibility(View.GONE);
        layoutPreparation.setVisibility(View.VISIBLE);
        layoutTravel.setVisibility(View.VISIBLE);

        chronoActiveIntervention.startTimer();
        chronoActiveTravel.stopTimer();
        chronoActivePreparation.stopTimer();
        chronoTravel.setTime(chronoActiveTravel.getTime());
        chronoPreparation.setTime(chronoActivePreparation.getTime());
        startTracking();

    }

    public void phasePause(View view) {
        updateWorkingPeriods(PAUSE);

        state = PAUSE;
        Log.d(TAG, "PAUSE !!  State => " + state);
        layoutActivePreparation.setVisibility(View.GONE);
        layoutActiveTravel.setVisibility(View.GONE);
        layoutActiveIntervention.setVisibility(View.GONE);
        layoutPreparation.setVisibility(View.VISIBLE);
        layoutTravel.setVisibility(View.VISIBLE);
        layoutIntervention.setVisibility(View.VISIBLE);

        chronoGeneral.stopTimer();
        chronoActivePreparation.stopTimer();
        chronoActiveTravel.stopTimer();
        chronoActiveIntervention.stopTimer();
        chronoPreparation.setTime(chronoActivePreparation.getTime());
        chronoTravel.setTime(chronoActiveTravel.getTime());
        chronoIntervention.setTime(chronoActiveIntervention.getTime());
        stopTracking();
    }

    @Override
    public void onBackPressed() {
        exitInterface();
    }

    private void saveAndExit() {

        if (BuildConfig.DEBUG)
            Log.i(TAG, "=== saveAndExit()");
        if (mNewIntervention && !newIntervStarted)
            return;
        phasePause(null);
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);

        dialog.setMessage(R.string.what_to_do);

        // First dialog finish button action
        dialog.setPositiveButton(R.string.finish, (dialogInterface, i) -> {

            boolean shouldDisplay = false;
            for (GenericItem tractor : tractors) {
                if (tractor.hasHourMeter) {
                    shouldDisplay = true;
                    break;
                }
            }

            if (shouldDisplay) {

                // Get pixel density
                final float scale = getResources().getDisplayMetrics().density;
                final int margin = (int) (20 * scale);

                // Hour counter dialog content
                final AlertDialog.Builder secondDialog = new AlertDialog.Builder(this);
                secondDialog.setTitle("Compteur horaire final");
                if (BuildConfig.FLAVOR.equals(LARRERE))
                    secondDialog.setMessage("Vous devez renseigner la valeur définitive du compteur horaire pour les équipements :");
                else
                    secondDialog.setMessage("Vous pouvez renseigner la valeur du compteur horaire pour les équipements :");

                // Content layout
                LinearLayout layout = new LinearLayout(secondDialog.getContext());
                layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                layout.setOrientation(LinearLayout.VERTICAL);

                // Build TextInput for each tractor
                for (GenericItem tractor : tractors) {

                    // Generate a view ID and store it to the tractor object for later
                    int viewId = View.generateViewId();
                    tractor.workNumber = String.valueOf(viewId);

                    // Creates new TextInputLayout (material design)
                    TextInputLayout textInputLayout = new TextInputLayout(secondDialog.getContext(), null);
                    textInputLayout.setId(viewId);
                    textInputLayout.setPadding(margin, margin, margin, 0);
                    textInputLayout.setHint(tractor.name);
                    textInputLayout.setHelperTextEnabled(true);
                    textInputLayout.setHelperText("en heures");
                    // Add related EditText
                    TextInputEditText editText = new TextInputEditText(textInputLayout.getContext());
                    editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                    String value = tractor.hourMeter != null
                            && tractor.hourMeter.compareTo(new BigDecimal(0)) > 0 ?
                            tractor.hourMeter.toString() : null;
                    editText.setText(value);
                    textInputLayout.addView(editText);
                    // Add a hidden warning text
//                TextView warningTextView = new TextView(builder.getContext());
//                warningTextView.setId(View.generateViewId());
//                tractor.number = String.valueOf(warningTextView.getId());
//                warningTextView.setText("Vous devez renseigner le compteur horaire.");
//                warningTextView.setTextColor(getResources().getColor(R.color.basic_red));
//                warningTextView.setVisibility(View.GONE);

                    // Add the views to the linearLayout
                    layout.addView(textInputLayout);
//                layout.addView(warningTextView);

                    // Add a text watcher to actualize finish button availability
                    editText.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        }

                        @Override
                        public void afterTextChanged(Editable editable) {
                            tractor.hourMeter = editable.length() > 0 ? new BigDecimal(editable.toString()) : null;
                            setButtonAvailability();
                        }
                    });
                }

                // Set the dialogBuilder view
                secondDialog.setView(layout);

                // Set event related to finish button
                secondDialog.setPositiveButton(R.string.finish, (builderInterface, n) -> {

                    for (GenericItem tractor : tractors) {

                        TextInputLayout inputLayout = layout.findViewById(Integer.valueOf(tractor.workNumber));
                        EditText editText = inputLayout.getEditText();
                        String hours = editText != null ? editText.getText().toString() : null;

                        if (hours != null && !hours.isEmpty()) {
                            Uri uri;
                            String[] args;
                            String whereClause;
                            ContentValues cv = new ContentValues();
                            cv.put(
                                    DetailedInterventionAttributes.HOUR_METER,
                                    Float.valueOf(hours));

                            if (detailedInterventionID != -1) {
                                uri = DetailedInterventionAttributes.CONTENT_URI;
                                whereClause = "detailed_intervention_id = ? AND reference_id = ?";
                                args = new String[]{String.valueOf(detailedInterventionID), String.valueOf(tractor.id)};
                            } else {
                                uri = InterventionParameters.CONTENT_URI;
                                whereClause = "fk_intervention = ? AND product_id = ?";
                                args = new String[]{String.valueOf(mInterventionID), String.valueOf(tractor.id)};
                            }

                            cr.update(uri, cv, whereClause, args);
                        }
                    }

                    safeDestroy = true;
                    switchStateToFinished();
                    InterventionActivity.super.onBackPressed();
                });

                hourMeterDialog = secondDialog.create();
                hourMeterDialog.show();
                setButtonAvailability();

            } else {
                safeDestroy = true;
                switchStateToFinished();
                InterventionActivity.super.onBackPressed();
            }
        });

        dialog.setNeutralButton(R.string.cancel, (dialogInterface, i) -> dialogInterface.cancel());

        if (!mNewIntervention) {
            dialog.setNegativeButton(R.string.pause, (dialogInterface, i) -> {
                safeDestroy = true;
                saveCurrentStateOfIntervention();
                InterventionActivity.super.onBackPressed();
            });
        }
        dialog.create();
        dialog.show();
    }

    private void setButtonAvailability() {
        hourMeterDialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(verifyHourMeters());
    }

    private boolean verifyHourMeters() {
        if (BuildConfig.FLAVOR.equals(LARRERE)) {
            for (GenericItem tractor : tractors)
                if (tractor.hourMeter == null || tractor.hourMeter.compareTo(new BigDecimal(0)) <= 0)
                    return false;
        }
        return true;
    }

    private void exitInterface() {
        if (mNewIntervention && !newIntervStarted) {
            super.onBackPressed();
            return;
        }
        phasePause(null);
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage(R.string.what_to_do);
        dialog.setPositiveButton(R.string.abort, (dialogInterface, i) -> {
            safeDestroy = true;
            cleanCurrentSave();
            finish();
//            InterventionActivity.super.onBackPressed();
        });
        dialog.setNeutralButton(R.string.cancel, (dialogInterface, i) -> dialogInterface.cancel());

        dialog.create();
        dialog.show();
    }

    private void cleanCurrentSave() {

        cr.delete(WorkingPeriods.CONTENT_URI,
                WorkingPeriods.FK_INTERVENTION + " == " + mInterventionID,
                null);
        cr.delete(Crumbs.CONTENT_URI,
                Crumbs.FK_INTERVENTION + " == " + mInterventionID,
                null);

//        ContentValues values = new ContentValues();
//        values.put(Interventions.GENERAL_CHRONO, 0);
//        values.put(Interventions.INTERVENTION_CHRONO, 0);
//        values.put(Interventions.PREPARATION_CHRONO, 0);
//        values.put(Interventions.TRAVEL_CHRONO, 0);
//        values.put(Interventions.REQUEST_COMPLIANT, 1);
//        values.put(Interventions.STATE, "ABANDONED");
//        cr.update(Interventions.CONTENT_URI, values,
//                mInterventionID + " == " + Interventions._ID,null);

        cr.delete(Interventions.CONTENT_URI, "type IS NULL AND _id == " + mInterventionID, null);
    }

    private void switchStateToFinished() {
        ContentValues values = new ContentValues();

//        values.put(Interventions.STATE, detailedInterventionID == -1 ? STATUS_FINISHED : DETAILED);
        values.put(Interventions.NAME, mLastProcedureNatureName);
        values.put(Interventions.STATE, STATUS_FINISHED);
        values.put(Interventions.REQUEST_COMPLIANT, diffStuff.isChecked());
        if (description != null && !description.equals(""))
            values.put(Interventions.DESCRIPTION, description);

        cr.update(Interventions.CONTENT_URI, values,
                Interventions._ID + " == " + mInterventionID, null);

        if (detailedInterventionID != -1) {
            values.clear();
            values.put(DetailedInterventions.STATUS, FINISHED);
            if (description != null && !description.equals(""))
                values.put(DetailedInterventions.DESCRIPTION, description);
            cr.update(ContentUris.withAppendedId(DetailedInterventions.CONTENT_URI,
                    detailedInterventionID), values, null, null);
        }
    }



    private void saveCurrentStateOfIntervention() {
        ContentValues values = new ContentValues();

        values.put(Interventions.GENERAL_CHRONO, chronoGeneral.getTime());
        values.put(Interventions.PREPARATION_CHRONO, chronoPreparation.getTime());
        values.put(Interventions.TRAVEL_CHRONO, chronoTravel.getTime());
        values.put(Interventions.INTERVENTION_CHRONO, chronoIntervention.getTime());
        values.put(Interventions.STATE, STATUS_PAUSE);
        values.put(Interventions.REQUEST_COMPLIANT, diffStuff.isChecked());
        if (description != null && !description.equals(""))
            values.put(Interventions.DESCRIPTION, description);

        cr.update(Interventions.CONTENT_URI, values,
                Interventions._ID + " == " + mInterventionID, null);

        if (detailedInterventionID != -1 && description != null && !description.equals("")) {
            values.clear();
            values.put(DetailedInterventions.DESCRIPTION, description);
            cr.update(ContentUris.withAppendedId(DetailedInterventions.CONTENT_URI,
                    detailedInterventionID), values, null, null);
        }

    }


    @Override
    public void onDestroy() {
        if (!safeDestroy)
            saveCurrentStateOfIntervention();
        stopIntervention(null);
        super.onDestroy();
    }

    private void createProcedureChooser() {
        infoScroll.setVisibility(View.GONE);
        mProcedureChooser = new AlertDialog.Builder(this)
                .setTitle(R.string.procedure_nature)
                .setNegativeButton(android.R.string.cancel, null)
                .setItems(R.array.procedures_entries, (dialog, which) -> {
                    mLastProcedureNature = getResources().getStringArray(R.array.procedures_values)[which];
                    mLastProcedureNatureName = getResources().getStringArray(R.array.procedures_entries)[which];
                    Log.d("zero", "Start a new " + mLastProcedureNature);
                    crumbsCalculator = new CrumbsCalculator(mLastProcedureNature);

                    mStartButton.setVisibility(View.GONE);
                    mMapButton.setVisibility(View.GONE);

                    chronoGeneral.setVisibility(View.VISIBLE);
                    layoutPreparation.setVisibility(View.VISIBLE);
                    layoutTravel.setVisibility(View.VISIBLE);
                    layoutIntervention.setVisibility(View.VISIBLE);
                    mProcedureNature.setText(mLastProcedureNatureName);

                    setTitle(mLastProcedureNatureName);
                    setNotification();

                    newIntervStarted = true;
                    createIntervention();
                });
    }

    private void setNotification() {
        mNotificationBuilder
                .setSmallIcon(R.mipmap.ic_stat_notify_running)
                .setContentTitle(mLastProcedureNatureName)
                .setContentText(getString(R.string.running));
        mNotificationManager.notify(mNotificationID, mNotificationBuilder.build());
    }

    private void createIntervention() {
        ContentValues values = new ContentValues();

        values.put(Interventions.USER, AccountTool.getCurrentAccount(this).name);
        values.put(Interventions.EK_ID, -1);
        values.put(Interventions.UUID, UUID.randomUUID().toString());
        values.put(Interventions.TYPE, "record");
        values.put(Interventions.PROCEDURE_NAME, mLastProcedureNature);

        values.put(Interventions.STARTED_AT, DateConstant.getCurrentDateFormatted());

        cr.insert(Interventions.CONTENT_URI, values);
        Cursor cursor = cr.query(Interventions.CONTENT_URI, new String[]{Interventions._ID}, null, null, null);
        if (cursor == null || !cursor.moveToLast())
            return;
        mInterventionID = cursor.getInt(0);
        cursor.close();
    }

    public void startIntervention(View view) {
        mProcedureChooser.show();
    }

    public void openMap(View view) {
        if (mNewIntervention && !newIntervStarted)
            return;
        if (!PermissionManager.internetPermissions(this, this)
                || !PermissionManager.storagePermissions(this, this)
                || !PermissionManager.GPSPermissions(this, this)) {
            Toast.makeText(this, R.string.gps_disabled, Toast.LENGTH_SHORT).show();
            return;
        }
        Intent intent = new Intent(this, MapsActivity.class);
        intent.putExtra(_interventionID, mInterventionID);
        startActivity(intent);
    }

    public void stopIntervention(View view) {
        this.stopTracking();

        if (mNewIntervention) {
            ContentValues values = new ContentValues();
            values.put(Interventions.STOPPED_AT, DateConstant.getCurrentDateFormatted());
            cr.update(Interventions.CONTENT_URI, values,
                    mInterventionID + " == " + Interventions._ID,null);
        }
        /*        int lastCrumbID = query_last_crumb_id();
        if (PermissionManager.GPSPermissions(this, this) && lastCrumbID != 0)
        {
            ContentValues values = new ContentValues();
            values.put(ZeroContract.Crumbs.TYPE, "stop");
            getContentResolver().update(ZeroContract.Crumbs.CONTENT_URI,
                    values,
                    ZeroContract.CrumbsColumns._ID + " == " + lastCrumbID,
                    null);
        }*/
        setTitle(R.string.new_intervention);
        mNotificationBuilder
                .setSmallIcon(R.mipmap.ic_stat_notify)
                .setContentTitle(getString(R.string.app_name))
                .setContentText("");
        mNotificationManager.cancel(mNotificationID);
    }

    private int query_last_crumb_id() {
        String[] projectionCrumbID = {Crumbs._ID};

        Cursor cursorCrumb = cr.query(
                Crumbs.CONTENT_URI,
                projectionCrumbID,
                null,
                null,
                Crumbs._ID + " DESC LIMIT 1");
        if (cursorCrumb == null || cursorCrumb.getCount() == 0)
            return (0);
        cursorCrumb.moveToFirst();
        int ret = cursorCrumb.getInt(0);
        cursorCrumb.close();
        return (ret);
    }

//    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
//        super.onActivityResult(requestCode, resultCode, intent);
//        IntentResult aScanResult = mScanIntegrator.parseActivityResult(requestCode, resultCode, intent);
//        if (aScanResult != null) {
//            final String contents = aScanResult.getContents();
//            if (contents != null) {
//                // TODO: Ask for quantity
//                // mBarcode.setText("CODE: " + contents);
//
//                // handle scan result
//                final Bundle metadata = new Bundle();
//                metadata.putString("scanned_code", aScanResult.getFormatName() + ":" + contents);
//                this.addCrumb("scan", metadata);
//            }
//        }
//    }

    private void startTracking() {
        startTracking(0);
    }

    @SuppressLint("MissingPermission")
    private void startTracking(long interval) {
        Log.d(TAG, "===Strat Tracking !===");
        if (!PermissionManager.GPSPermissions(this, this))
            return;
        mLocationManager.requestLocationUpdates(mLocationProvider, interval, 0, mTrackingListener);
    }

    private void stopTracking() {
        Log.d(TAG, "===Stop Tracking !===");
        if (!PermissionManager.GPSPermissions(this, this))
            return;
        mLocationManager.removeUpdates(mTrackingListener);
    }

    private void addCrumb(String type) {
        this.addCrumb(type, null);
    }

    @SuppressLint("MissingPermission")
    private void addCrumb(String type, Bundle metadata) {
        if (!PermissionManager.GPSPermissions(this, this))
            return;
        TrackingListener listener = new TrackingListener(this, type, metadata);
        mLocationManager.requestSingleUpdate(mLocationProvider, listener, null);
    }

    public void writeCrumb(Location location, String type, Bundle metadata) {

        if (!crumbsCalculator.isCrumbAccurate(location, type, metadata)) {
            gpsAccuracyMessage.setText(R.string.bad_accuracy_message);
            return;
        }

        Crumb crumb = crumbsCalculator.getFinalCrumb();
        putCrumbOnLocalDatabase(crumb);
        sendBroadcastNewCrumb(location);

        gpsAccuracyMessage.setText(R.string.good_accuracy_message);
    }

    private void sendBroadcastNewCrumb(Location location) {
        Intent intent = new Intent(UpdatableActivity.PING);
        intent.putExtra(TrackingListenerWriter.LATITUDE, location.getLatitude());
        intent.putExtra(TrackingListenerWriter.LONGITUDE, location.getLongitude());
        intent.putExtra(_interventionID, mInterventionID);
        sendBroadcast(intent);
    }

    private void pingMap() {
        Intent intent = new Intent(UpdatableActivity.PING);
        intent.putExtra(_interventionID, mInterventionID);
        sendBroadcast(intent);
    }

    private void putCrumbOnLocalDatabase(Crumb crumb) {
        ContentValues values = new ContentValues();

        values.put(Crumbs.USER, AccountTool.getCurrentAccount(this).name);
        values.put(Crumbs.TYPE, crumb.getType());
        values.put(Crumbs.LATITUDE, crumb.getLatitude());
        values.put(Crumbs.LONGITUDE, crumb.getLongitude());
        values.put(Crumbs.READ_AT, crumb.getDate());
        values.put(Crumbs.ACCURACY, 0);
        values.put(Crumbs.SYNCED, 0);
        values.put(Crumbs.FK_INTERVENTION, mInterventionID);

        cr.insert(Crumbs.CONTENT_URI, values);
    }
}
