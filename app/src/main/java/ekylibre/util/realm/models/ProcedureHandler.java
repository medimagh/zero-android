package ekylibre.util.realm.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ProcedureHandler extends RealmObject {

    public static final String tableName = "ProcedureHandler";

    @PrimaryKey
    public String name;
    public String indicator;
    public String unit;

}
