package ekylibre.util.realm.models;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Procedure extends RealmObject {

    public static final String tableName = "Procedure";

    @PrimaryKey
    public String name;
    public RealmList<String> categories;
    public RealmList<String> optionalActions;
    public RealmList<ProcedureAttribute> targets;
    public RealmList<ProcedureAttribute> inputs;
    public RealmList<ProcedureAttribute> outputs;
    public RealmList<ProcedureAttribute> doers;
    public RealmList<ProcedureAttribute> tools;
    public String group;
    public boolean deprecated;

}
