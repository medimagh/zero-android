package ekylibre.util.realm.models;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ProcedureAttribute extends RealmObject {

    public static final String tableName = "ProcedureAttribute";

    @PrimaryKey
    public String name;
    public String filter;
    public String cardinality;
    public String group;
    public RealmList<ProcedureHandler> handlers;

}
