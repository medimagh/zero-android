package ekylibre.util;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import static ekylibre.util.Helper.getTranslation;

public class Unit {

    public String name;
    public String label;
    public String dimension;
    public String dimensionLabel;
    public boolean surfaceRelated;
    public String baseUnit;
    public int factorToBaseUnit;

    public Unit(String name, String dimension, boolean surfaceRelated, String baseUnit, int factorToBaseUnit) {
        this.name = name;
        this.label = getTranslation(name);
        this.dimension = dimension;
        this.dimensionLabel = getTranslation(dimension);
        this.surfaceRelated = surfaceRelated;
        this.baseUnit = baseUnit;
        this.factorToBaseUnit = factorToBaseUnit;
    }

    @NonNull
    @Override
    public String toString() {
        return String.format("%s (%s)", label, dimensionLabel);
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof Unit)
            return ((Unit) obj).name.equals(this.name);
        else
            return false;
    }
}
