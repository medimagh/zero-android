package ekylibre.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;

import ekylibre.zero.BuildConfig;
import ekylibre.zero.R;

/**************************************
 * Created by pierre on 8/4/16.       *
 * ekylibre.zero.util for zero-android*
 *************************************/

/*
** Extend classic activity that listen sync events and ping event
** You can override methods needed to do what you want on event
** The generic application toolbar can be easily added by calling setTollBar
*/
public abstract class UpdatableActivity extends AppCompatActivity {

    private static final String TAG = "UpdatableActivity";
    public static final String ACTION_STARTED_SYNC = "ekylibre.zero.util.ACTION_STARTED_SYNC";
    public static final String ACTION_PROGRESS_SYNC = "ekylibre.zero.util.ACTION_PROGRESS_SYNC";
    public static final String ACTION_FINISHED_SYNC = "ekylibre.zero.util.ACTION_FINISHED_SYNC";
    public static final String PING = "ekylibre.zero.util.PONG";

//    private static IntentFilter syncIntentFilterFINISHED;
//    private static IntentFilter syncIntentFilterSTART;
//    private static IntentFilter syncIntentFilterPROGRESS;
//    private static IntentFilter pingIntentFilter;
//
//    private BroadcastReceiver syncBroadcastReceiverFinish;
//    private BroadcastReceiver syncBroadcastReceiverStart;
//    private BroadcastReceiver syncBroadcastReceiverProgress;
//    private BroadcastReceiver pingBroadcastReceiver;

    protected Toolbar toolbar;
    protected boolean isSync;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (BuildConfig.DEBUG)
            Log.d(TAG, "onCreate()");

        isSync = false;

//        syncIntentFilterSTART = new IntentFilter(ACTION_STARTED_SYNC);
//        syncIntentFilterFINISHED = new IntentFilter(ACTION_FINISHED_SYNC);
//        syncIntentFilterPROGRESS = new IntentFilter(ACTION_PROGRESS_SYNC);
//        pingIntentFilter = new IntentFilter(PING);
//
//        // Init BroadcastReceivers
//        syncBroadcastReceiverStart = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                if (BuildConfig.DEBUG)
//                    Log.d(TAG, "I'm receiving start message from syncAdapter");
//                SharedPreferences prefs = getSharedPreferences("prefs", Context.MODE_MULTI_PROCESS);
//
//                if (!prefs.getBoolean("isSyncing", false)) {
//                    onSyncStart();
//                    isSync = true;
//                }
//            }
//        };
//
//        syncBroadcastReceiverFinish = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                if (BuildConfig.DEBUG)
//                    Log.d(TAG, "I'm receiving finish message from syncAdapter");
//                isSync = false;
//                onSyncFinish();
//            }
//        };
//
//        syncBroadcastReceiverProgress = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                String message = intent.getStringExtra("message");
//                int progress = intent.getIntExtra("progress", 0);
//                int max = intent.getIntExtra("max", 0);
//
//                if (max == 0)
//                    onSyncProgress(message);
//                else
//                    onSyncProgressBar(progress, max);
//            }
//        };
//
//        pingBroadcastReceiver = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                if (BuildConfig.DEBUG)
//                    Log.d(TAG, "I'm receiving ping message from" + context.toString());
//                onPing(intent);
//            }
//        };
    }

    /**
    ** Use this method to apply the application toolbar
    ** You MUST have called onCreate() AND setContentView()
    ** to use this method
    */
    protected void setToolBar() {
        Toolbar tb = findViewById(R.id.toolbar);

        setSupportActionBar(tb);
        if (tb == null || getSupportActionBar() == null)
            return;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar = tb;
    }

    /**
    ** Called when sync is finished
    */
    protected void onSyncFinish(){
        if (BuildConfig.DEBUG)
            Log.e(TAG, "onSyncFinish");
    }

    /**
    ** Called when sync starts
    */
    protected void onSyncStart(){
        if (BuildConfig.DEBUG)
            Log.d(TAG, "onSyncStart");
    }

    /**
     ** Called when sync starts
     */
    protected void onSyncProgress(String message){
    }

    protected void onSyncProgressBar(int progress, int max){
    }

    /**
    ** Called when ping is sent
    */
    protected void onPing(Intent intent){}

    @Override
    protected void onStart() {
        super.onStart();
        if (BuildConfig.DEBUG)
            Log.i(TAG, "onStart()");
    }

    @Override
    protected void onResume() {
        super.onResume();

//        if (BuildConfig.DEBUG)
//            Log.d(TAG, "Register Broadcast receivers");
//        try {
//            registerReceiver(syncBroadcastReceiverFinish, syncIntentFilterFINISHED);
//            registerReceiver(syncBroadcastReceiverStart, syncIntentFilterSTART);
//            registerReceiver(syncBroadcastReceiverProgress, syncIntentFilterPROGRESS);
//            registerReceiver(pingBroadcastReceiver, pingIntentFilter);
//        } catch (Exception e) {
//            e.printStackTrace();
//            Log.e(TAG, "Broadcast already registered");
//        }
    }

    @Override
    protected void onPause() {
//        try {
//            unregisterReceiver(syncBroadcastReceiverFinish);
//            unregisterReceiver(syncBroadcastReceiverStart);
//            unregisterReceiver(syncBroadcastReceiverProgress);
//            unregisterReceiver(pingBroadcastReceiver);
//        } catch (Exception e) {
//            e.printStackTrace();
//            Log.e(TAG, "Broadcast already unregistered");
//        }
        super.onPause();
    }
}
