package ekylibre.APICaller;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import ekylibre.exceptions.HTTPException;


public class DetailedIntervention {

    public static JSONObject create(Instance instance, JSONObject attributes)
            throws JSONException, IOException, HTTPException {

        return instance.post("/api/v1/interventions", attributes);
    }
}
