![App logo](/app/src/main/res/mipmap-xxxhdpi/ic_launcher.png)

# Zero for Android  
  
_Zero_ is the _Ekylibre_ client app for _Android_.

## Develop environment configuration

1. Be sure to have already downloaded and installed latest [Android Studio](https://developer.android.com/studio) for your platforme (linux recommended).
2. Clone the zero-android repository in a place you're familiar working with (_e.g._, your Project folder) :
```
git clone git@gitlab.com:ekylibre/zero-android.git
```
3. Copy the `gradle.properties` file (from _Ekylibre_ Drive `Tech > Zero`) into the project root folder.
4. Copy the `sentry.properties` file (from _Ekylibre_ Drive `Tech > Zero > Sentry`) into the project root folder.
5. Open _Android Studio_ and select the command `Import project (Gradle, Eclipse ADT, etc.)` and pick up the project root folder (_e.g._, zero-android).
6. If required, _Android Studio_ will ask you to install few SDK tools and other package, accept until _Gradle_ compiles everythings without errors.

## Flavor switching

This app is designed to be customized according to several **flavors**. 
Be aware to choose the wanted flavor before building the project. Go to the menu entry `Build > Select Build Variant...` and select the right one.
You can switch whenever you want while developing the app.

## Troubleshooting

You may experience error with Android Virtual Devices (AVD) on certain plateform/configuration. Below is a list proposing some solutions :
- KVM problem : [https://blog.chirathr.com](https://blog.chirathr.com/android/ubuntu/2018/08/13/fix-avd-error-ubuntu-18-04/)